const express = require('express');
const app = express();

const path = require('path');

const settings = require('./server/config/settings');
const dbConnect = require('./server/utils/dbConnect');

require('./server/startup/tools')(app);
require('./server/startup/routes')(app);


if (process.env.NODE_ENV === 'production') {

    app.use(express.static('client/build'));

    app.get('*', (req, res) => {

        res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))

    })

}


const port = process.env.PORT || 5000;


const run = async () => {
    try {
        await dbConnect(settings.db);
        app.listen(port, () => {
            console.log(`DB connected!\nServer listen on port ${port}`);
        })
    } catch (ex) {
        console.log('Connection failed!\n', ex);
    }
}

run();