const { v2: { uploader }, config } = require('cloudinary');
const { cloudinary: cloudinaryConfig } = require('../config/settings');

config(cloudinaryConfig);

const uploadFile = (file, filter = { width: 1200, height: 900 }) => new Promise((resolve, reject) => {
  const imgTypesPattern = new RegExp('png|jpg|jpeg');

  if (!imgTypesPattern.test(file.mimetype)) {
    return reject(new Error('Недопустимый формат файла!'));
  }

  uploader
    .upload_stream(
      { 
        resourse_type: 'raw',
        transformation: [
          { ...filter, quality: "auto", crop: "crop", gravity: "auto" }
        ]
      },
      (error, result) => {
        error ? reject(error) : resolve(result.secure_url)
      }
    )
    .end(file.buffer)
});

module.exports.uploadImages = (filter = { width: 1200, height: 900 }) => {

  return async (req, res, next) => {
    

    try {
      console.log('REQ FILES: ', req.files);
      req.savedFiles = (req.files && req.files.length) ? await Promise.all(
        req.files.map(file => uploadFile(file, filter))
      ) : [];
  
      next();
    } catch (ex) {
      res.status(400).json({ success: false, errors: { uploadFiles: ex.message } })
    }
  }

}

module.exports.uploadImagesFromForm = async (files, filter = { width: 225, height: 900 }) => {
    files = (files && files.length) ? await Promise.all(
      files.map(file => uploadFile(file, filter))
    ) : [];

    return files;
}