const _ = require('lodash');
const User = require('../models/User');

/**
 * 
 */
exports.ensureHaveRightsOf = acceptableRoles => async (req, res, next) => {
    const { userId } = req;

    console.log('CHECK RIGHTS MW RECEIVED USER ID: ', userId);

    if (!userId) return res.status(404).json({ success: false, errors: { user: 'пользователь не найден' } });

    const user = await User.findOne({ _id: userId }).select('-password');

    console.log('CHECK RIGHTS MW RECEIVED USER: ', user);

    // user.roles.forEach( role => {
    //     if (acceptableRoles.includes(role)) {
    //         req.user = user;
    //         return next();
    //     }
    // } );

    if (user.roles.some( role => acceptableRoles.includes(role) )) {

        req.user = user;
        return next();

    } else {

        res.status(403).json({ success: false, errors: { access: 'доступ запрещен' } });

    }

    
}
