const jwt = require('jsonwebtoken');
const { jwtSecret } = require('../config/settings')

/** 
 * @middleware
 * @description check header for token, if it exists, than return user data in @var req.user to next middleware
 */
module.exports = async (req, res, next) => {
    const token = req.header('x-auth-token');

    if (!token) return res.status(401).json({ success: false, errors: { token: 'not_found' } });


    try {
        const tokenData = jwt.verify(token, jwtSecret);

        req.userId = tokenData.userId;
        next();
    } catch (err) {
        console.log('ERROR VERIFICATION IN AUTH MW: ', err)
        res.status(401).json({ success: false, errors: { token: 'not_valid' } });
    }

}