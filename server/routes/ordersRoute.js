const router = require('express').Router();
const { addNewOrder, getOrdersList } = require('../controllers/OrdersController');

const auth = require('../middlewares/auth');
const { ensureHaveRightsOf } = require('../middlewares/checkRights');

router.get('/', auth, ensureHaveRightsOf('admin'), getOrdersList);

router.post('/send-message', addNewOrder);

module.exports = router;