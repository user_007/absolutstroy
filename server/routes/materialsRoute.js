const router = require('express').Router();

const { 
  getAllMaterials, getWallsList, getBasementsList, getRoofsList, addNewMaterial, removeMaterial, editMaterial 
} = require('../controllers/MaterialsController');

const auth = require('../middlewares/auth');
const { ensureHaveRightsOf } = require('../middlewares/checkRights');

router.get('/', getAllMaterials);

router.get('/walls', getWallsList);
router.get('/roofs', getRoofsList);
router.get('/basements', getBasementsList);

router.post(
  '/add', 
  auth, ensureHaveRightsOf('admin'), addNewMaterial
);

router.put(
  '/:materialId/update',
  auth, ensureHaveRightsOf('admin'), editMaterial
);

router.delete(
  '/:materialId/delete',
  auth, ensureHaveRightsOf('admin'), removeMaterial
);


module.exports = router;
