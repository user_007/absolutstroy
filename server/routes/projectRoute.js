const router = require('express').Router();
const auth = require('../middlewares/auth');
const { ensureHaveRightsOf } = require('../middlewares/checkRights');

const { 
    addNewProject, getListOfProjects, getProjectData, editProject, removeProject
} = require('../controllers/ProjectsController');

const { uploadImages } = require('../middlewares/imageProcessing');
const uploadFiles = require('../middlewares/uploadFile');


router.get('/', getListOfProjects);
router.get('/:slug', getProjectData);
router.get('/:id/get-data', getProjectData);

router.post('/add-new-project', uploadFiles, uploadImages({ width: 1200, height: 900 }), addNewProject);
router.post('/:projectId/update', uploadFiles, uploadImages({ width: 1200, height: 900 }), editProject);

router.delete('/:projectId/remove', auth, ensureHaveRightsOf('admin'), removeProject);

module.exports = router;
