const router = require('express').Router();
const {
  addNewPhoto, getListOfPhotoes, editPhoto, removePhoto, countAllPhotoes
} = require('../controllers/GalleryController');
const auth = require('../middlewares/auth');
const { ensureHaveRightsOf } = require('../middlewares/checkRights');
const uploadFiles = require('../middlewares/uploadFile');
const { uploadImages } = require('../middlewares/imageProcessing');

router.get('/', getListOfPhotoes);

router.get('/count', countAllPhotoes);

//todo: ensure user has admin rights
router.post(
  '/add-new-photo', 
  auth, ensureHaveRightsOf('admin'), uploadFiles, uploadImages({ width: 1200, height: 900 }), addNewPhoto
);

router.post(
  '/:photoId/edit',
  auth, ensureHaveRightsOf('admin'), uploadFiles, uploadImages({ width: 1200, height: 900 }), editPhoto
)

router.post(
  '/remove-photo',
  auth, ensureHaveRightsOf('admin'), removePhoto
)

module.exports = router;
