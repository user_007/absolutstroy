const router = require('express').Router();
const auth = require('../middlewares/auth');

const { userAuthentication, addNewUser } = require('../controllers/UserController');
const { ensureHaveRightsOf } = require('../middlewares/checkRights');

router.get(
  '/get-user-data', 
  auth, ensureHaveRightsOf('admin'),
  (req, res) => res.status(200).json({ success: true, user: req.user })
)

router.post('/login', userAuthentication);
router.post('/add', addNewUser);

module.exports = router;