const { createTransport } = require('nodemailer');
const { email: { login: user, pass, address } } = require('../config/settings');

const transporter = createTransport({
  host: 'smtp.yandex.ru',
  port: 465,
  secure: true, // true for 465, false for other ports
  auth: {
      user, // generated ethereal user
      pass // generated ethereal password
  }
});

exports.sendingMail = async (opts) => {
  return await transporter.sendMail({
    from: `${address} <${address}>`,
    to: opts.to,
    subject: opts.subject = 'Добрыйдень',
    html: opts.msg
  })
}