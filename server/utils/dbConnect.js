const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

let dbState = {
    connection: null
}

const connect = async (url) => {
    if (!dbState.connection) {
        dbState.connection = await mongoose.connect(
            url, { useNewUrlParser: true, useCreateIndex: true }
        )
    }
}

module.exports = connect;