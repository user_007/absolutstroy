let localSettings = process.env.NODE_ENV === 'production' ? {} : require('../../config/settings.local');

module.exports = {
    db: process.env.NODE_ENV === 'production' ? process.env.DB_URL : localSettings.db,
    cloudinary: {
        cloud_name: process.env.NODE_ENV === 'production' ? process.env.CLOUDINARY_CLOUD : localSettings.cloudinary.cloudName,
        api_key: process.env.NODE_ENV === 'production' ? process.env.CLOUDINARY_KEY : localSettings.cloudinary.apiKey,
        api_secret: process.env.NODE_ENV === 'production' ? process.env.CLOUDINARY_SECRET : localSettings.cloudinary.secret
    },
    email: {
        login: process.env.NODE_ENV === 'production' ? process.env.EMAIL_LOGIN : localSettings.email.login,
        pass: process.env.NODE_ENV === 'production' ? process.env.EMAIL_PASSWORD : localSettings.email.pass,
        address: process.env.NODE_ENV === 'production' ? process.env.EMAIL_ADDRESS : localSettings.email.address
    },
    jwtSecret: process.env.NODE_ENV === 'production' ? process.env.JWT_SECRET : localSettings.jwtSecret
}