const projectRoute = require('../routes/projectRoute');
const materialsRoute = require('../routes/materialsRoute');
const usersRoute = require('../routes/usersRoute');
const galleryRoute = require('../routes/galleryRoute');
const ordersRoute = require('../routes/ordersRoute');


module.exports = app => {
    app.use('/api/projects', projectRoute);
    app.use('/api/materials', materialsRoute);
    app.use('/api/users', usersRoute);
    app.use('/api/gallery', galleryRoute);
    app.use('/api/orders', ordersRoute);
}