const mongoose = require('mongoose');

const projectSchema = new mongoose.Schema({
    title: {
        type: String,
        minlength: 5,
        required: true,
        unique: true
    },
    category: {
        type: String,
        require: true
    },
    slug: {
        type: String,
        default: 'page-not-found'
    },
    //todo: it will be better to change type from String to Photo, 
    //but my client doesn't want to spend time on that at this moment
    photoes: [String],
    features: {
        basement: String,
        walls: String,
        roof: String,
        area: Number,
        floors: Number,
        height: Number,
        timeToBuild: Number
    },
    price: Number,
    description: String
});

// projectSchema.statics.getCategoriesList = () => {
//     return projectSchema.path('category').enumValues;
// }


module.exports = mongoose.model('projects', projectSchema);
