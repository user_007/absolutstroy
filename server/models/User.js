const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        default: 'Стандартный Админ'
    },
    login: {
        type: String,
        default: 'admin'
    },
    password: {
        type: String,
        // 'admin'
        default: '$2y$12$w0XV4bHoaUcyIraX5NYIPuvexIZacJgiIkSXYbR5w9hjxE2xkLylu'
    },
    roles: {
        type: [String],
        enum: ['admin', 'guest', 'ban', 'user'],
        default: ['guest']      
    },
    email: String,
    dateRegister: {
        type: Date,
        default: Date.now
    }
})


module.exports = mongoose.model('users', userSchema);
