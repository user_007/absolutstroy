const mongoose = require('mongoose');

const materialSchema = new mongoose.Schema({
    material: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true,
        enum: ['wall', 'roof', 'basement']
    },
    price: {
        type: Number,
        required: true
    },
    image: String,
    description: {
        type: String,
        default: ''
    }
});


module.exports = mongoose.model('materials', materialSchema);
