const mongoose = require('mongoose');

const photoSchema = new mongoose.Schema({
  title: String,
  src: {
    type: String,
    required: true
  },
  loadingDate: {
    type: Date,
    default: Date.now
  },
  comment: String
});

module.exports = mongoose.model('photoes', photoSchema);
