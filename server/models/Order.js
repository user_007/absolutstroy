const mongoose = require('mongoose');
const validator = require('validator');

const orderSchema = new mongoose.Schema({
    person: {
        type: String,
        required: true,
        minlength: 3
    },
    phone: {
        type: String,
        required: true
    },
    orderType: {
        type: String,
        required: true,
        enum: ['calc', 'message', 'callback', 'project'],
        default: 'message'
    },
    comment: {
        type: String
    },
    details: String,
    sendTime: {
        type: Date,
        default: Date.now
    },
    isRead: {
        type: Boolean,
        default: false
    }
});


orderSchema.methods.orderCheck = function() {
    let errors = {};
    const { person, phone } = this;

    // const namePattern = new RegExp('^([a-zA-Zа-яА-Я]){3, 60}$');
    // const mobilePhonePattern = new RegExp('^((\+7|7|8)+([0-9]){10})$');
    console.log('DATA TO VALIDATE: ', phone, person)

    if (!person || person.length < 3 || person.length > 60) {
        errors.person = 'имя должно быть от 3 до 60 букв';
    }

    if (!phone || !validator.isMobilePhone(phone, 'ru-RU')) errors.phone = 'неверный формат телефона';

    if (Object.keys(errors).length > 0) return { isValid: false, errors };

    return { isValid: true, errors: null };
}


module.exports = mongoose.model('orders', orderSchema);
