const Project = require('../models/Project');
const slug = require('limax');
const _ = require('lodash')


module.exports.addNewProject = async (req, res) => {
    const { category, price, description, title, ...featuresFields } = req.body;
    const projectData = {
        category, price, description, title, 
        features: {
            ...featuresFields
        },
        photoes: req.savedFiles || []
    }

    console.log('Get to ADD NEW PROJECT CONTROLLER!')

    // temporary break point for checking received data, when new project form submited
    // console.log('Received data: ', projectData);

    try {
        const nameExists = await Project.findOne({ title });

        console.log('Checking existing title: ', nameExists)

        if (nameExists) return res.status(400).json({ success: false, errors: { title: 'Проект с таким именем уже существует' } });

        projectData.slug = slug(title, { lang: 'ru' });

        const newProject = new Project({ ...projectData });

        console.log('Creating new project entity: ', newProject)

        await newProject.save();

        console.log('Project saved!')

        res.status(200).json({ success: true, errors: null });
    } catch (ex) {
        console.log('Error in add new project')
        res.status(500).json({ success: false, errors: ex.message || ex });
    }
}


module.exports.editProject = async (req, res) => {
    const { category, price, description, title, oldFiles, ...featuresFields } = req.body;

    const { projectId } = req.params;
    const projectData = {
        category, price, description, title, 
        features: {
            ...featuresFields
        },
        photoes: !!req.savedFiles ? [ ...req.savedFiles, ...oldFiles.split(',') ] : [ ...oldFiles.split(',') ]
    }


    res.status(200).json({ success: true, project: projectData });

    try {
        await Project.findOneAndUpdate( { _id: projectId }, { ...projectData });

        res.status(200).json({ success: true })
    } catch (ex) {
        res.status(400).json({ success: false, errors: ex.message || ex })
    }
}




module.exports.getListOfProjects = async (req, res) => {
    let {limit, skip, ...filters} = req.query;
    let query = {};
    
    if (!_.isEmpty(filters)) query = {
        'price': { $gte: filters.min_price || 0, $lte: filters.max_price || 60000000 },
        'features.height': { $gte: filters.min_height, $lte: filters.max_height },
        'features.floors': { $gte: filters.min_floors, $lte: filters.max_floors }
    };

    if (filters.walls && filters.walls !== 'Все типы') query['features.walls'] = filters.walls;
    if (filters.roof && filters.roof !== 'Все типы') query['features.roof'] = filters.roof;
    if (filters.basement && filters.basement !== 'Все типы') query['features.basement'] = filters.basement;


    console.log('FILTERS: ', filters);
    console.log('QUERY: ', query);

    try {

        const projectsList = await Project.find(
            query
        )
        .select('-description')
        .limit(Number(limit) || 12)
        .skip(Number(skip) || 0);

        res.status(200).json({ success: true, projectsList });

    } catch (ex) {

        res.status(400).json({ success: false, errors: ex })
        
    }
    
}


module.exports.getProjectData = async (req, res) => {
    const { id, slug } = req.params;

    try {

        const project = !!id ? await Project.findOne({ _id: id }) : await Project.findOne({ slug });

        if (!project) return res.status(404).json({ success: false, errors: { project: 'Проект не найден' } });

        res.status(200).json({ success: true, project });
        
    } catch (ex) {
        
        res.status(500).json({ success: false, errors: ex.message || ex });

    }
    
}


module.exports.removeProject = async (req, res) => {
    const { projectId } = req.params;

    if (!projectId) 
        return res.status(400).json({ success: false, errors: { projectId: 'проект не найден' } });

    try {
        
        await Project.findByIdAndRemove(projectId);

        res.status(200).json({ success: true });

    } catch (err) {
        
        res.status(400).json({ success: false, errors: { deleteRequest: err } });

    }
}
