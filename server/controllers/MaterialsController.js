const Material = require('../models/Material');

/**
 * form body:
 * type, material, price, image
 */
module.exports.addNewMaterial = async (req, res) => {
    const { ...materialData } = req.body;

    const newMaterial = new Material(materialData);
    try {

        await newMaterial.save();
        res.status(200).json({ success: true, newMaterial })
    } catch (ex) {
        res.status(400).json({ success: false, errors: ex.message || ex });
    }
}


module.exports.editMaterial = async (req, res) => {
    const materialData = req.body;
    const { materialId } = req.params;

    try {
        await Material.findOneAndUpdate(
            { _id: materialId },
            { ...materialData }
        )

        res.status(200).json({ success: true });
    } catch (ex) {
        res.status(400).json({ success: false, errors: ex.message || ex })
    }
}


module.exports.removeMaterial = async (req, res) => {
    const { materialId } = req.params;
    console.log('ID of material expected to be deleted: ', materialId)

    try {
        await Material.findOneAndDelete({ _id: materialId });

        res.status(200).json({ success: true })
    } catch (ex) {
        console.log(ex)
        res.status(400).json({ success: false, errors: ex.message || ex })
    }
}


module.exports.getAllMaterials = async (req, res) => {
    try {
        const materialsList = await Material.find({});
        res.status(200).json({ success: true, materialsList });
    } catch (ex) {
        res.status(400).json({ success: false, errors: ex.message || ex });
    }
}


module.exports.getBasementsList = async (req, res) => {
    try {
        const basementsList = await Material.find({ type: 'basement' }).select('material price -_id');
        // const basementsList = basements.map(basement => {
        //     return basement.material;
        // })

        res.status(200).json({ success: true, basementsList });
    } catch (ex) {
        res.status(400).json({ success: false, errors: ex.message || ex });
    }
}


module.exports.getRoofsList = async (req, res) => {
    try {
        const roofsList = await Material.find({ type: 'roof' }).select('material price -_id');
        // const roofsList = roofs.map(roof => {
        //     return roof.material;
        // })

        res.status(200).json({ success: true, roofsList });
    } catch (ex) {
        res.status(400).json({ success: false, errors: ex.message || ex });
    }
}


module.exports.getWallsList = async (req, res) => {

    try {
        const wallsList = await Material.find({ type: 'wall' }).select('material price -_id');
        // const wallsList = walls.map(wall => {
        //     return wall.material;
        // })

        res.status(200).json({ success: true, wallsList });
    } catch (ex) {
        res.status(400).json({ success: false, errors: ex.message || ex });
    }
}
