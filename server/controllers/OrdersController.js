const Order = require('../models/Order');
const mail = require('../utils/mail');

module.exports.addNewOrder = async (req, res) => {
    const { type, details, ...orderData } = req.body;

    // if (details) orderData.details = details;

    const newOrder = new Order({...orderData, orderType: type, details: details || ''});

    const validation = newOrder.orderCheck();

    // return res.status(200).json({ success: true, data: { type, calcData, orderData } });


    if (!validation.isValid) return res.status(400).json({ success: false, errors: validation.errors });

    try {
        await newOrder.save();

        const message = newOrder.comment || 'комментарий отсутствует';
        const orderType = {
            calc: 'расчеты из калькулятора',
            message: 'сообщение',
            callback: 'заказ обратного звонка',
            project: 'заказ проекта'
        }

        const orderDetails = details 
            ? details
            : '' 

        const opts = {
            to: 'ip.nezus@yandex.ru',
            subject: `Новый заказ - ${orderType[type]}`,
            msg: `<div>
            <h3 style="font-weight:bold;font-size:16px;">Поступил новый заказ!</h3>
            <ul style="font-size:14px;list-style:none;padding:0">
                <li>Имя отправителя: <b>${newOrder.person}</b></li>
                <li>Телефон: <b>${newOrder.phone}</b></li>
            </ul>
            <br />
            
            <h3 style="font-weight:bold;font-size:16px;margin-bottom:5px;">Комментарий к заказу:</h3>
            <p style="margin:0;">${message}</p><br /><br />${orderDetails}`
          }
        
        await mail.sendingMail(opts);

        res.status(200).json({ success: true });
    } catch (ex) {
        res.status(400).json({ success: false, errors: ex.message || ex });
    }
}

module.exports.getOrdersList = async (req, res) => {
    
    try {
        const ordersList = await Order.find({});

        res.status(200).json({ success: true, ordersList });
    } catch (ex) {
        res.status(500).json({ success: false, errors: ex.message || ex })
    }

}
