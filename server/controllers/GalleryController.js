const Photo = require('../models/Photo');

module.exports.getListOfPhotoes = async (req, res) => {
  const params = req.query;

  console.log('PARAMS: ', params);

  try {

    const photoes = await Photo.find()
      .limit(Number(params.limit) || 20000)
      .skip(Number(params.skip) || 0)
      .sort('-loadingDate');

    res.status(200).json({ success: true, photoes });

  } catch (err) {

    res.status(400).json({ success: false, errors: err });
    
  }

}


module.exports.countAllPhotoes = async (req, res) => {

  try {
    
    const photoesAmount = await Photo.estimatedDocumentCount();

    res.status(200).json({ success: true, photoesAmount: photoesAmount })

  } catch (err) {
    
    res.status(400).json({ success: false, errors: { err } })

  }

}


module.exports.addNewPhoto = async (req, res) => {

  if (!req.savedFiles) return res.status(400).json({ success: false, errors: { fileNotFound: 'фотография небыла загружена на сервер' } });

  const data = {
    src: req.savedFiles[0],
    comment: req.body.comment || '',
    title: req.body.title || ''
  }

  try {

    const newPhoto = new Photo(data);

    await newPhoto.save();

    res.status(200).json({ success: true, photo: newPhoto });

  } catch (err) {

    res.status(500).json({ success: false, errors: { server: err } });
    
  }

}


module.exports.editPhoto = async (req, res) => {
  const { photoId } = req.params;
  let data = {
    comment: req.body.comment
  };

  if (req.savedFiles) data.src = req.savedFiles[0];

  try {

    const result = await Photo.findOneAndUpdate(
      { _id: photoId }, 
      { ...data }
    );

    res.status(200).json({ success: true, editedPhoto: result });

  } catch (err) {

    res.status(500).json({ success: false, errors: { server: err } });
    
  }
}


module.exports.removePhoto = async (req, res) => {
  const listOfIds = req.body;

  try {
    
    const result = await Photo.remove({ _id: listOfIds });

    if (!result) return res.status(400).json({ success: false, errors: { deleteFromDb: 'Не удалось удалить выбранные фотографии' } });

    res.status(200).json({ success: true, data: result });

  } catch (err) {

    res.status(500).json({ success: false, errors: { server: err } })
    
  }
}
