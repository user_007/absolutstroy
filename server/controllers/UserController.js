const User = require('../models/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { jwtSecret } = require('../config/settings');
const _ = require('lodash');


exports.userAuthentication = async (req, res) => {
    const { password, login } = req.body;


    try {

        const user = await User.findOne({ login });

        if (!user) {
            return res.status(400).json({ success: false, errors: 'Неверные данные' });
        }

        const correctPassword = await bcrypt.compare(password, user.password);
        

        if (!correctPassword) {
            return res.status(400).json({ success: false, errors: 'Неверные данные' });
        }

        const token = jwt.sign(
            { userId: user.id },
            jwtSecret,
            { expiresIn: 3 * 3600 }
        )

        res.status(200).json({ success: true, token });

    } catch (err) {

        res.status(400).json({ success: false, errors: err })

    }
}


exports.addNewUser = async (req, res) => {
    const { password, password2, login, email, name } = req.body;
    let errors = {};

    if (password !== password2) return res.status(400).json({ success: false, errors: { passwordsNotEqual: 'пароли не совпадают' } });

    const [loginExists, emailExists] = await Promise.all([
        User.find({ login }).countDocuments(),
        User.find({ email }).countDocuments()
    ]);

    if (loginExists) errors.login = `логин ${login} уже занят`;
    if (emailExists) errors.email = `пользователь с почтой ${email} уже существует`;

    if (!_.isEmpty(errors)) return res.status(400).json({ success: false, errors });


    try {
        const salt = await bcrypt.genSalt(12);
        const hashedPassword = await bcrypt.hash(password, salt)

        const newUser = new User({
            password: hashedPassword, login, email, name, roles: ['admin']
        });

        newUser.save();

        const token = jwt.sign({ userId: newUser.id }, jwtSecret, { expiresIn: 3 * 3600 });
        // const token = jwt.sign({ user: newUser }, jwtSecret, { expiresIn: 3 * 3600 });

        res.status(200).json({ success: true, token });
    } catch (err) {
        res.status(500).json({ success: false, errors: err });
    }
}


exports.deleteUser = async (req, res) => {
    const { id } = req.params;

    try {
        await User.findOneAndDelete({ _id: id });

        res.status(200).json({ success: true });
    } catch (err) {
        res.status(400).json({ success: false, errors: err });
    }
}

