import React from "react";
import { Route, Redirect } from "react-router-dom";
import { checkUserRights } from "../actions/authActions";

const ProtectedRoute = ({ path, component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={
        props => {

          checkUserRights(['admin'])
            .then(isAllowed => {
              return isAllowed ? <Component {...props} /> : <Redirect to="/admin/login" />
            })


        }
      }
    />
  );
};

export default ProtectedRoute;
