export const loadFileWithPreview = file => new Promise((resolve, reject) => {
  const reader = new FileReader();

  let fileObject = {
    file,
    preview: null
  };

  reader.readAsDataURL(file);

  reader.onload = e => {
    fileObject.preview = e.target.result;
  }

  reader.onloadend = e => {
    resolve(fileObject)
  } 

  reader.onerror = e => {
    reject(e)
  }

});


export const verySimpleCheckIsHandHeldDevice = () => window.innerWidth < 768;


export const formatPhone = phone => {
  const phoneParts = phone.split('+');
  
  return phoneParts[phoneParts.length - 1].replace(/(\d{1})(\d{3})(\d{3})(\d{2})(\d{2})/, '+$1\xa0($2)\xa0$3-$4-$5');
}
