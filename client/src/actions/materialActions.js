import axios from 'axios';

export const getListOfBasements = async () => {
    try {
        const resp = await axios.get('/api/materials/basements');

        return resp.data.basementsList;
    } catch (ex) {
        return { error: ex }
    }
}

export const getListOfRoofs = async () => {
    try {
        const resp = await axios.get('/api/materials/roofs');

        return resp.data.roofsList;
    } catch (ex) {
        return { error: ex }
    }
}

export const getListOfWalls = async () => {
    try {
        const resp = await axios.get('/api/materials/walls');

        return resp.data.wallsList;
    } catch (ex) {
        return { error: ex }
    }
}

export const getListOfAllMaterials = async () => {
    try {
        const resp = await axios.get('/api/materials');

        return resp.data;
    } catch (err) {
        return err.response.data;
    }
}

export const addNewMaterial = async materialData => {
    try {
        const response = await axios.post(`/api/materials/add`, materialData);

        return response.data;
    } catch (err) {
        return err.response;
    }
}

export const updateMaterial = async materialData => {
    const { id, ...data } = materialData;

    try {
        const response = await axios.put(`/api/materials/${id}/update`, data);

        return response.data;
    } catch (err) {
        return err.response;
    }
}

export const removeMaterial = async materialId => {
    try {
        const response = await axios.delete(`/api/materials/${materialId}/delete`);

        return response.data
    } catch (err) {
        return err.response;
    }
}
