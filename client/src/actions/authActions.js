import axios from 'axios';
import setAuthHeader from '../utils/setAuthHeader';

export const userAuthorization = async ({ login, password }) => {
  const data = new FormData();

  data.append('login', login);
  data.append('password', password);

  try {
    const response = await axios.post('/api/users/login', { login, password }, { headers: { 'content-type': 'application/json' } });

    localStorage.setItem('sidToken', response.data.token);
    setAuthHeader(response.data.token)

    return response.data;
  } catch (err) {
    
    return err.response.data;

  }
}


export const getUserData = async () => {
  
  try {
    const auth = await axios.get('/api/users/get-user-data');

    return auth.data;
  } catch (err) {
    return err.response.data;
  }

}


export const checkUserRights = async ( rights ) => {

  try {

    const response = await getUserData();

    const { user: { roles } } = response;

    const intersected = roles.some( role => rights.includes(role) );

    return intersected;
    
  } catch (err) {

    return false;
    
  }

}
