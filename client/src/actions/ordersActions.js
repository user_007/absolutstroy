import axios from 'axios';



export const getListOfOrders = async () => {
  try {
    
    const response = await axios.get('/api/orders');

    return response.data;

  } catch (err) {

    return err.response;
    
  }
}


export const sendOrder = async data => {
  try {
      const response = await axios.post('/api/orders/send-message', data);

      return response.data;
  } catch (err) {
      return err.response;
  }
}
