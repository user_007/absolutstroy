import axios from 'axios';

export const addNewPhoto = async photoObject => {

  const data = new FormData();
  data.append('files', photoObject.file);
  data.append('title', photoObject.title || '');
  data.append('comment', photoObject.comment || '');
  
  
  try {

    const result = await axios.post('/api/gallery/add-new-photo', data);

    return result.data;
    
  } catch (err) {

    return err.response;
    
  }

}

export const getGalleryItems = async ({ limit, skip = 0 }) => {
  
  try {
    
    const response = await axios.get(`/api/gallery?limit=${limit}&skip=${skip}`);
    
    return response.data;

  } catch (err) {

    return err.response;
    
  }
}

export const getSizeOfGallery = async () => {

  try {
    
    const response = await axios.get('/api/gallery/count');

    return response.data;

  } catch (err) {
    
    return err.response;

  }

}

export const editPhotoComment = async (id, comment) => {

  try {
    
    return await axios.post(`/api/gallery/${id}/edit`, { comment });

  } catch (err) {
    
    return err.response.data;

  }

}


export const removePhoto = async photosIdList => {
  try {

    const response = await axios.post('/api/gallery/remove-photo', photosIdList, { 'content-type': 'application/json' });

    return response.data;
    
  } catch (err) {

    return err.response;
    
  }
}
