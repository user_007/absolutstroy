import axios from 'axios';

export const saveNewProject = async projectData => {
    try {
        const response = await axios.post('/api/projects/add-new-project', projectData);

        return response.data;
    } catch (ex) {
        console.log('Error in save new project Action: ', ex.response.data)
        return ex.response;
    }
}

export const editProject = async (projectId, projectData) => {
    try {
        const response = await axios.post(`/api/projects/${projectId}/update`, projectData, { 'content-type': 'application/json' });

        return response.data;
    } catch (err) {
        console.log('Error in saving changes to existing project Action: ', err.response.data)
        return err.response;
    }
}

export const getListOfProjects = async (filters = '') => {
    try {
        const response = await axios.get(`/api/projects?${filters.toString()}`);
        
        return response.data;
    } catch (err) {
        const { status, data } = err.response;

        return { status, data };
    }
}


export const getProjectData = async slug => {
    try {
        const response = await axios.get(`/api/projects/${slug}`);
        return response.data;

    } catch (err) {
        console.error('Error occured while fetching project data: ', err);
        return err.response;
    }
}


export const getProjectById = async id => {
    try {
        const response = await axios.get(`/api/projects/${id}/get-data`);
        return response.data;

    } catch (err) {
        console.error('Error occured while fetching project data: ', err);
        return err.response;
    }
}


export const deleteProject = async projectId => {
    try {

        const response = await axios.delete(`/api/projects/${projectId}/remove`);

        return response.data;
        
    } catch (err) {

        return err.response;

    }
}
