import houseImg from '../assets/images/house4-2.jpg';
import logo from '../assets/images/abs-stroy-logo.png';
import brus from '../assets/images/categories/Brus.jpg';
import gazobeton from '../assets/images/categories/Gazobeton.jpg';
import sip from '../assets/images/categories/sip-panel.jpg';
import calc from '../assets/images/calc/calc3.png';
import calcBg from '../assets/images/calc/calc-bg.png';

import tehnonikol from '../assets/images/brands/technonikol.jpg';
import starateli from '../assets/images/brands/starateli.png';
import ursa from '../assets/images/brands/ursa.png';
import tikkurila from '../assets/images/brands/tikkurila.jpg';
import shinglas from '../assets/images/brands/shinglas.jpg';
import docke from '../assets/images/brands/docke-logo.png';
import kaparol from '../assets/images/brands/caparol.jpg';
import ceresit from '../assets/images/brands/ceresit.jpg';
import knauf from '../assets/images/brands/knauf.jpg';
import rockwool from '../assets/images/brands/rockwool.png';

import roof from '../assets/images/svg/roof.svg';
import wall from '../assets/images/svg/wall.svg';

import top from '../assets/images/calc/calc-top.png';
import middle from '../assets/images/calc/calc-mid.png';
import bottom from '../assets/images/calc/calc-bottom.png';

export default {
  HOUSE: houseImg,
  LOGO: logo,
  BRUS: brus,
  GAZOBETON: gazobeton,
  SIP: sip,
  CALC: calc,
  CALC_BG: calcBg,
  BRAND_ROCKWOOL: rockwool,
  BRAND_KNAUF: knauf,
  BRAND_CERESIT: ceresit,
  BRAND_TEKNONIKOL: tehnonikol,
  BRAND_STARATELI: starateli,
  BRAND_URSA: ursa,
  BRAND_TEKKURILA: tikkurila,
  BRAND_SHINGLAS: shinglas,
  BRAND_DOCKE: docke,
  BRAND_KAPAROL: kaparol,
  Svg: {
    ROOF: roof,
    WALL: wall
  },
  Calc: {
    TOP: top,
    MIDDLE: middle,
    BOTTOM: bottom
  }
}