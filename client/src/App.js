import React, { Component } from 'react';
import { Route, BrowserRouter as Router, Switch, Redirect } from 'react-router-dom';

import setAuthHeader from './utils/setAuthHeader';

import 'bootstrap/dist/css/bootstrap.css';
import './fonts.css';
import './App.css';

import LoginPage from './components/pages/admin/LoginPage';
// import ProtectedRoute from './utils/ProtectedRoute';

import Header from './components/HeaderComponent';
import Footer from './components/FooterComponent';
import MainPage from './components/pages/MainPageComponent';
import Gallery from './components/pages/GalleryPage';
import ProjectList from './components/pages/ProjectsListComponent';
import ProjectPage from './components/pages/ProjectPageComponent';
import ContactPage from './components/pages/ContactPage';

import AdminMainPage from './components/pages/admin/AdminMainPage';
import AdminMaterialsSection from './components/pages/admin/AdminMaterialsSection';
import AdminOrdersSection from './components/pages/admin/AdminOrdersSection';
import AdminNewProject from './components/pages/admin/AdminProjectSectionPage';
import AdminSidebar from './components/pages/admin/AdminSidebarComponent';
import AdminProjectsList from './components/pages/admin/AdminProjectsListSection';
import AdminGallery from './components/pages/admin/AdminGallerySection';

import ServerErrorPage from './components/pages/errors/ServerErrorPage';
import PageNotFound from './components/pages/errors/NotFoundPage';


class App extends Component {
  render() {

    if (localStorage.sidToken) {
      setAuthHeader(localStorage.sidToken);
    }

    return (
      <Router>

        <Switch>

          <Route path="/admin/login" component={null} />
          <Route path="/error" component={null} />
          <Route path="/admin" component={AdminSidebar} />
          <Route component={Header} />

        </Switch>

        <Switch>

          <Route exact path="/" component={MainPage} />
          <Route exact path="/gallery" component={Gallery} />
          <Route exact path="/projects/" component={ProjectList} />
          <Route exact path="/projects/:slug" component={ProjectPage} />
          <Route exact path="/contacts" component={ContactPage} /> 

          <Route exact path="/admin" component={AdminMainPage} />
          <Route exact path="/admin/login/" component={LoginPage} />
          <Route exact path="/admin/gallery/" component={AdminGallery} />
          <Route exact path="/admin/projects/" component={AdminProjectsList} />
          <Route exact path="/admin/projects/:projectId/edit" component={AdminNewProject} />
          <Route exact path="/admin/projects/new" component={AdminNewProject} />
          <Route exact path="/admin/orders" component={AdminOrdersSection} />
          <Route exact path="/admin/materials" component={AdminMaterialsSection} />

          <Route exact path="/error/server-error" component={ServerErrorPage} />
          <Route exact path="/error/page-not-found" component={PageNotFound} />

          <Redirect to="/error/page-not-found" />

        </Switch>

        <Switch>

          <Route path="/admin" component={null} />
          <Route path="/error" component={null} />
          <Route component={Footer} />

        </Switch>

      </Router>
    );
  }
}


export default App;
