import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { slide as Menu } from 'react-burger-menu';
import _ from 'lodash';
import toaster from 'toasted-notes';

import data from '../temp/main-page-categories';

import Modal from './utils/Modal';
import OrderForm from './forms/OrderFormComponent';

import { sendOrder } from '../actions/ordersActions';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStoreAlt, faGlobe, faImages, faHome } from '@fortawesome/free-solid-svg-icons';

import Image from '../temp/images';

import './Header.css';

class HeaderComponent extends Component {

  state = {
    orderPending: false,
    showOrderForm: false,
    menuIsOpen: false,
    errors: {},
    person: '',
    phone: '',
    comment: ''
  }


  handleChangeFieldsOfModalOrderForm = e => {

    const { errors } = this.state;

    this.setState({
      [e.target.name]: e.target.value,
      errors: _.omit(errors, [e.target.name])
    })
  }
  
  
  handleSubmitModalOrderForm = async e => {
    e.preventDefault();

    const { orderPending, showOrderForm, errors, ...order } = this.state;

    this.setState({ orderPending: true });

    const result = await sendOrder({ ...order, type: 'callback' });

    let notification;

    if (!result.success) {

      const { data: { errors } } = result;

      if (errors && ( errors.person || errors.phone ))
        return this.setState({ 
          orderPending: false,
          errors: errors
        });

      notification = 'Не удалось отправить заявку на обратный звонок';

    } else {

      notification = 'Заявка на обратный звонок отправлена. Мы свяжемся с Вами в ближайшее время.';

    }

    this.setState({ 
      orderPending: false,
      person: '',
      phone: '',
      comment: '',
      showOrderForm: false
    });

    toaster.notify(notification, { duration: 5000, position: 'bottom-right' });

  }

  handleMenuStateChange (state) {
    this.setState({ menuIsOpen: state.isOpen })  
  }


  chooseMenuItem = () => {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
    this.setState({ menuIsOpen: false })
  }


  render() {
    const { person, phone, comment, showOrderForm, menuIsOpen } = this.state;

    return (
      <div className="header">
        <div className="mob-menu">
          <Menu isOpen={ menuIsOpen } onStateChange={ state => this.handleMenuStateChange(state) } >
            <Link to='/' className="nav-menu__link" onClick={() => this.chooseMenuItem()} >
              <FontAwesomeIcon className="mr-2" icon={faHome} /> главная
            </Link>
            <Link to='/projects' className="nav-menu__link" onClick={() => this.chooseMenuItem()} >
              <FontAwesomeIcon className="mr-2" icon={faStoreAlt} /> проекты
                </Link>
            <Link to='/gallery' className="nav-menu__link" onClick={() => this.chooseMenuItem()} >
              <FontAwesomeIcon className="mr-2" icon={faImages} /> фотогалерея
                </Link>
            <Link to='/contacts' className="nav-menu__link" onClick={() => this.chooseMenuItem()} >
              <FontAwesomeIcon className="mr-2" icon={faGlobe} /> контакты
            </Link>
          </Menu>
        </div>
        <div className="container">
          <div className="top-line">            
            <Link className="logo" to="/">
              <img src={Image.LOGO} alt="логотип компании Абсолют Строй 92" className="logo__img" />
              {/* <h1 className="co-title">Абсолют Строй</h1> */}
            </Link>
            <h1 className="slogan">Строительство домов и котеджей под ключ в Крыму и Севастополе.</h1>
            <div className="contacts">
              <ul className="phones-list">
                <li className="phone">{ data.contacts.phone1 }</li>
                <li className="phone">{ data.contacts.phone2 }</li>
              </ul>
              <button 
                className="btn-info btn"
                onClick={ () => this.setState({ showOrderForm: true }) }
              >
                Заказать звонок
              </button>
            </div>
          </div>
        </div>
        <nav className="nav-menu  position-sticky">
          <div className="container">

            <ul className="nav-menu__list">
              <li className="nav-menu__item">
                <Link to='/' className="nav-menu__link">
                  <FontAwesomeIcon className="mr-2" icon={faHome} /> главная
                </Link>
              </li>
              <li className="nav-menu__item">
                <Link to='/projects' className="nav-menu__link">
                  <FontAwesomeIcon className="mr-2" icon={faStoreAlt} /> проекты
                </Link>
              </li>
              {/* <li className="nav-menu__item">
                <Link to='/materials' className="nav-menu__link">
                  <FontAwesomeIcon className="mr-2" icon={faTh} /> материалы
                </Link>
              </li> */}
              <li className="nav-menu__item">
                <Link to='/gallery' className="nav-menu__link">
                  <FontAwesomeIcon className="mr-2" icon={faImages} /> фотогалерея
                </Link>
              </li>
              <li className="nav-menu__item">
                <Link to='/contacts' className="nav-menu__link">
                  <FontAwesomeIcon className="mr-2" icon={faGlobe} /> контакты
                </Link>
              </li>
              <li className="nav-menu__item tools"></li>
            </ul>
          </div>
        </nav>

        <Modal 
          isOpen={ showOrderForm }
          handleModalClose={ () => this.setState({ showOrderForm: false }) }
        >
          <OrderForm 
            title="Заказать звонок"
            errors={ this.state.errors }
            alert="Укажите Ваше имя и телефон, чтобы наши специалисты смогли связаться с Вами в ближайшее время"
            fields={ {person, phone, comment} }
            handleChange={ this.handleChangeFieldsOfModalOrderForm }
            handleSubmit={ this.handleSubmitModalOrderForm }
          />
        </Modal>

      </div>
    )
  }
}

export default HeaderComponent;
