import React from 'react';
import { Link } from 'react-router-dom';

import Icon from '../icons/Icon';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faRubleSign } from '@fortawesome/free-solid-svg-icons';

import './BuildingCard.css';

const BuildingCard = project => {

  if (!project || Object.keys(project).length < 3) return null;

  const { 
    // eslint-disable-next-line
    category, title, price, slug, _id: id, 
    // eslint-disable-next-line
    photoes: [imgSrc, ...otherPhotoes], 
    features: {
      walls, roof, timeToBuild
    } 
  } = project;

  return (
    <div className="ft-recipe">
      <div className="ft-recipe__thumb">
        <h3>{category}</h3>
        <img 
          src={ imgSrc }
          alt={ title } 
        />
        <p className="project-price"><FontAwesomeIcon icon={ faRubleSign } /> { price }</p>
      </div>
      <div className="ft-recipe__content">
        <header className="content__header">
          <div className="row-wrapper">
            <h2 className="recipe-title"><Link to={`/projects/${slug}`}>{`Проект: "${title}"`}</Link></h2>            
          </div>
          <ul className="recipe-details">
            <li className="recipe-details-item time">
              <small className="recipe-details__title">монтаж</small>
              <span className="value">{ timeToBuild } дней</span>
              {/* <span className="title">дней</span> */}
            </li>
            <li className="recipe-details-item ingredients">
              <small className="recipe-details__title">гарантия</small>
              <span className="value">5 лет</span>
              {/* <span className="title">лет</span> */}
            </li>            
          </ul>
          <ul className="project-features">
            <li className="project-features-item">
              <Icon icon="roof" className="project-icon" /> Кровля: { roof }
            </li>
            <li className="project-features-item">
              <Icon icon="wall" className="project-icon" /> Стены: { walls }
            </li>
          </ul>
          
        </header>
      </div>
    </div>
  )
}

export default BuildingCard;
