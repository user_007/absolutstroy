import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt, faTags, faSpinner } from '@fortawesome/free-solid-svg-icons';

import './ProjectTableRow.css';

const AdminProjectTableRow = (
  {index, loading, handleRemove, project}
) => {
  const { title, category, price, _id: id } = project;

  const currency = new Intl.NumberFormat(
    'ru-RU', { style: 'currency', currency: 'RUB'}
  );

  // const editBtn = loading 
  //   ? <FontAwesomeIcon icon={ faSpinner } spin />
  //   : <FontAwesomeIcon icon={ faEdit } />

  const deleteBtn = loading 
    ? <FontAwesomeIcon icon={ faSpinner } spin />
    : <FontAwesomeIcon icon={ faTrashAlt } />
  

  return (
    <div className="project-table-row">
      <div className="ptr--index">{index}</div>
      <div className="ptr--info">
        <Link to={`/admin/projects/${id}/edit`} className="ptr--title">Проект "{title}"</Link>
        <small className="ptr--category"><FontAwesomeIcon icon={ faTags } /> категория: {category}</small>
      </div>
      <div className="ptr--price">{ currency.format(price) }</div>
      <div className="ptr--tools">
        {/* <button 
          className="btn-outline-info btn btn-sm ml-2 px-3"
          disabled={ loading }
          onClick={ loading ? null : () => handleEdit(id) }
        >
          {editBtn}
        </button> */}
        <button 
          className="btn-outline-danger btn btn-sm ml-2 px-3"
          disabled={ loading }
          onClick={ loading ? null : () => handleRemove(id) }
        >
          {deleteBtn}
        </button>
      </div>

    </div>
  )
}


AdminProjectTableRow.propTypes = {
  project: PropTypes.shape({
    title:    PropTypes.string.isRequired,
    category: PropTypes.string.isRequired,
    _id:       PropTypes.string,
    price:    PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired
  }),
  index:        PropTypes.number.isRequired,
  handleRemove: PropTypes.func.isRequired,
  loading:     PropTypes.bool.isRequired
}

AdminProjectTableRow.defaultProps = {
  loading: false
}

export default AdminProjectTableRow;
