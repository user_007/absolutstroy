import React from 'react';
import PropTypes from 'prop-types';

import moment from 'moment';

import { formatPhone } from '../../utils/commonFunctions';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { 
  faDraftingCompass, faEnvelopeOpenText, faCalculator, faPhoneAlt 
} from '@fortawesome/free-solid-svg-icons';

import './OrderRow.css';

const AdminOrderTableRow = (
  { index, onOpenDetails, order }
) => {
  const { person, phone, orderType, sendTime, isRead } = order;

  // const editBtn = loading 
  //   ? <FontAwesomeIcon icon={ faSpinner } spin />
  //   : <FontAwesomeIcon icon={ faEdit } />

  // ORDER fields:
    // _id: ObjectId,
    // person: String,
    // phone: String,
    // orderType: String = enum: ['calc', 'message', 'callback', 'project'],
    // comment:  String,
    // details: String,
    // sendTime: Date,
    // isRead: Boolean

    const icons = {
        calc: <div className="order-icon icon_calc"><FontAwesomeIcon icon={faCalculator} /></div>,
        message: <div className="order-icon icon_msg"><FontAwesomeIcon icon={faEnvelopeOpenText} /></div>,
        callback: <div className="order-icon icon_clbck"><FontAwesomeIcon icon={faPhoneAlt} /></div>,
        project: <div className="order-icon icon_project"><FontAwesomeIcon icon={faDraftingCompass} /></div>
    }

    const date = moment(sendTime).format('DD.MM.YYYY HH:mm');
  

  return (
    <div 
      className={`project-table-row${isRead ? null : ' not-read'}`} 
      onClick={ e => onOpenDetails(index) }
    >
      <div className="ptr--index">{icons[orderType]}</div>
      <div className="ptr--info">{person}</div>
      <div className="ptr--phone">{formatPhone(phone)}</div>
      { order.details && <div dangerouslySetInnerHTML={{ __html: order.details }}></div> }
      
      <div className="ptr--date">{date}</div>

    </div>
  )
}



AdminOrderTableRow.propTypes = {
  order: PropTypes.shape({
    person:     PropTypes.string.isRequired,
    phone:      PropTypes.string.isRequired,
    sendTime:   PropTypes.string.isRequired,
    orderType:  PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    isRead:     PropTypes.bool.isRequired
  }),
  onOpenDetails: PropTypes.func.isRequired
}

export default AdminOrderTableRow;
