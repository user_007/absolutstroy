import React from 'react'
import PropTypes from 'prop-types';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


function NotificationCard({
  person, phone, description, sendTime, preview
}) {
  return (
    <div className="notification_card">
      { preview && <div className="notification_icon"><FontAwesomeIcon icon="faEnvelopeOpenText" /></div> }
    </div>
  )
}

NotificationCard.propTypes = {
  persone: PropTypes.string.isRequired,
  phone: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  sendTime: PropTypes.string.isRequired,
  preview: PropTypes.bool.isRequired
}

Notification.defaultProps = {
  preview: false
}

export default NotificationCard;

