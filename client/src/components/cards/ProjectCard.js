import React from 'react';
import PropTypes from 'prop-types';

import './ProjectCard.css';

const ProjectCard = ({/*
  person, phone, description, sendTime, preview 
*/}) => {
  return (
    <div class="project-card barbarian">
      <div class="project-card__image project-card__image--barbarian">
        <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/195612/barbarian.png" alt="barbarian" />
      </div>
      <div class="project-card__level project-card__level--barbarian">Level 4</div>
      <div class="project-card__unit-name">The Barbarian</div>
      <div class="project-card__unit-description">
        The Barbarian is a kilt-clad Scottish warrior with an angry, battle-ready expression, hungry for destruction. He has Killer yellow horseshoe mustache.
      </div>

      <div class="project-card__unit-stats project-card__unit-stats--barbarian clearfix">
        <div class="one-third">
          <div class="stat">20<sup>S</sup></div>
          <div class="stat-value">Training</div>
        </div>

        <div class="one-third">
          <div class="stat">16</div>
          <div class="stat-value">Speed</div>
        </div>

        <div class="one-third no-border">
          <div class="stat">150</div>
          <div class="stat-value">Cost</div>
        </div>

      </div>

    </div>
  )
}

// ProjectCard.propTypes = {
//   persone: PropTypes.string.isRequired,
//   phone: PropTypes.string.isRequired,
//   description: PropTypes.string.isRequired,
//   sendTime: PropTypes.string.isRequired,
//   preview: PropTypes.bool.isRequired
// }

// ProjectCard.defaultProps = {
//   preview: false
// }

export default ProjectCard;
