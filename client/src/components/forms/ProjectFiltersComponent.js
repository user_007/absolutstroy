import React from 'react';
import PropTypes from 'prop-types';
import { v4 as genKey } from 'uuid';

import RangeSlider from 'react-input-range';

const ProjectFiltersComponent = ({
  filters, lists, limits, handleSubmit, handleChange, handleFieldValidation
}) => {

  const { basements, roofs, walls } = lists;

  const optionsBasements = basements.map((option, ind) => <option key={genKey()} value={ind}>{option.material}</option>);
    const optionsRoofs = roofs.map((option, ind) => <option key={genKey()} value={ind}>{option.material}</option>);
    const optionsWalls = walls.map((option, ind) => <option key={genKey()} value={ind}>{option.material}</option>);

  return (
    <div className="filters">
      <form onSubmit={ handleSubmit } className="filters_form">
        <div className="filters__header">
          <h4 className="filters__caption">Фильтры проектов</h4>
        </div>
        <div className="filters__body">
          <div className="filters__block">
            <div className="form-group">
              <label htmlFor="price" className="control-label filters__title">Цена:</label>
              <div className="rangeslider multi-input">
                <div className="multi-input__group">
                  <input
                    type="text" name="price-min"
                    className="form-control"
                    value={filters.price.min}
                    onChange={ e => {
                      const value = {
                        ...filters.price,
                        min: Number(e.target.value)
                      },
                      field = 'price';

                      return handleChange(value, field);
                    }}
                    onBlur={handleFieldValidation('price')}                    
                  />
                  <input
                    type="text" name="price-max"
                    className="form-control"
                    value={filters.price.max}
                    onChange={ e => {
                      const value = {
                        ...filters.price,
                        max: Number(e.target.value)
                      },
                      field = 'price';

                      return handleChange(value, field);
                    }}
                    onBlur={handleFieldValidation('price')}
                  />
                </div>
                <RangeSlider
                  name="price"
                  value={filters.price}
                  minValue={limits.price.min} maxValue={limits.price.max} step={1000}
                  onChange={val => handleChange(
                    { min: Number(val.min), max: Number(val.max) }, 
                    'price'
                  )}
                />
              </div>
            </div>
          </div>
          <div className="filters__block">
            <div className="form-group">
              <label htmlFor="floors" className="control-label filters__title">Кол-во этажей:</label>
              <div className="rangeslider multi-input">
                <div className="multi-input__group">
                  <input
                    type="text" name="floors-min"
                    className="form-control"
                    value={filters.floors.min}
                    onChange={ e => {
                      const value = {
                        ...filters.height,
                        min: Number(e.target.value)
                      },
                      field = 'floors';

                      return handleChange(value, field);
                    }}
                    onBlur={handleFieldValidation('floors')}
                  />
                  <input
                    type="text" name="floors-max"
                    className="form-control"
                    value={filters.floors.max}
                    onChange={ e => {
                      const value = {
                        ...filters.height,
                        max: Number(e.target.value)
                      },
                      field = 'floors';

                      return handleChange(value, field);
                    }}
                    onBlur={handleFieldValidation('floors')}
                  />
                </div>
                <RangeSlider
                  name="floors"
                  value={filters.floors}
                  minValue={limits.floors.min} maxValue={limits.floors.max} step={1}
                  onChange={val => handleChange(
                    { min: Number(val.min), max: Number(val.max) }, 
                    'floors'
                  )}
                />
              </div>
            </div>
          </div>
          <div className="filters__block">
            <div className="form-group">
              <label htmlFor="height" className="control-label filters__title">Высота потолков:</label>
              <div className="rangeslider multi-input">
                <div className="multi-input__group">
                  <input
                    type="text" name="height-min"
                    className="form-control"
                    value={filters.height.min}
                    onChange={ e => {
                      const value = {
                        ...filters.height,
                        min: Number(e.target.value).toFixed(2)
                      },
                      field = 'height';

                      return handleChange(value, field);
                    }}
                    onBlur={handleFieldValidation('height')}
                  />
                  <input
                    type="text" name="height-max"
                    className="form-control"
                    value={filters.height.max}
                    onChange={ e => {
                      const value = {
                        ...filters.height,
                        max: Number(e.target.value).toFixed(2)
                      },
                      field = 'height';

                      return handleChange(value, field);
                    }}
                    onBlur={handleFieldValidation('height')}
                  />
                </div>
                <RangeSlider
                  name="height"
                  value={filters.height}
                  minValue={limits.height.min} maxValue={limits.height.max} step={0.05}
                  onChange={val => handleChange(
                    { 
                      min: Number(val.min).toFixed(2), 
                      max: Number(val.max).toFixed(2) 
                    }, 
                    'height'
                  )}
                />
              </div>
            </div>
          </div>
          <div className="filters__block">
            <div className="form-group">
              <label htmlFor="walls" className="control-label filters__title">Стены:</label>
              <select
                name="walls" id="walls" className="form-control"
                value={filters.walls}
                onChange={e => handleChange(Number(e.target.value), 'walls')}
              >
                {optionsWalls}
              </select>
            </div>
          </div>
          <div className="filters__block">
            <div className="form-group">
              <label htmlFor="roof" className="control-label filters__title">Кровля:</label>
              <select
                name="roof" id="roof" className="form-control"
                value={filters.roof}
                onChange={e => handleChange(Number(e.target.value), 'roof')}
              >
                {optionsRoofs}
              </select>
            </div>
          </div>
          <div className="filters__block">
            <div className="form-group">
              <label htmlFor="basement" className="control-label filters__title">Фундамент:</label>
              <select
                name="basement" id="basement" className="form-control"
                value={filters.basement}
                onChange={e => handleChange(Number(e.target.value), 'basement')}
              >
                {optionsBasements}
              </select>
            </div>
          </div>
        </div>
        <div className="filters__footer">
          <div className="filters__control">
            <button className="btn btn-info btn-block">Применить</button>
          </div>
        </div>
      </form>
    </div>
  )
}



ProjectFiltersComponent.propTypes = {
  handleChange: PropTypes.func,
  handleSubmit: PropTypes.func,
  filters: PropTypes.shape({
    height: PropTypes.shape({
      min: PropTypes.number,
      max: PropTypes.number
    }),
    price: PropTypes.shape({
      min: PropTypes.number,
      max: PropTypes.number
    }),
    floors: PropTypes.shape({
      min: PropTypes.number,
      max: PropTypes.number
    }),
    walls: PropTypes.number,
    roof: PropTypes.number,
    basement: PropTypes.number
  }),
  lists: PropTypes.objectOf(PropTypes.array)
}

ProjectFiltersComponent.defaultProps = {
  filters: {}
}

export default ProjectFiltersComponent;
