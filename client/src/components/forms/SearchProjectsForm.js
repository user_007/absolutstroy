import React, { useState } from 'react';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { v4 as keyGen } from 'uuid';

import './OrderForm.css';



const SearchProjectsForm = ({ roofsList, wallsList, basementsList }) => {
    const [category, setCategory] = useState(0);
    const [roof, setRoof] = useState(0);
    const [basement, setBasement] = useState(0);
    const [maxPrice, setMaxPrice] = useState(0);


    const validateAndSetMaxPrice = e => {
        e.preventDefault();

        const price = Number(e.target.value);

        if (Number.isInteger(price)) setMaxPrice(price)
    }


    const handleSubmit = e => {
        e.preventDefault();

        // this.props.history.push({
        //     pathname: '/projects',
        //     filters: { maxPrice, roof, basement, walls: category }
        // })
    }


    const roofOptions = roofsList.map((roof, ind) => <option key={keyGen()} value={ind}>{roof}</option>);

    const basementOptions = basementsList.map((basement, ind) => <option key={keyGen()} value={ind}>{basement}</option>);

    const wallsOptions = wallsList.map((wall, ind) => <option key={keyGen()} value={ind}>{wall}</option>);



    return (
        <div className="form-project-search">
            <form onSubmit={handleSubmit} className="form-horizontal">
                <div className="row">
                    <div className="col-sm-6 col-md-3">
                        <div className="form-group">
                            <label className="control-label label-sm" htmlFor="category">Стены</label>
                            <select
                                name="category" id="category"
                                className="form-control input-sm"
                                value={category}
                                onChange={e => setCategory(e.target.value)}
                            >{wallsOptions}</select>
                        </div>
                    </div>

                    <div className="col-sm-6 col-md-3">
                        <div className="form-group">
                            <label className="control-label label-sm" htmlFor="basement">Фундамент</label>
                            <select
                                name="basement" id="basement"
                                className="form-control input-sm"
                                value={basement}
                                onChange={e => setBasement(e.target.value)}
                            >{basementOptions}</select>
                        </div>
                    </div>

                    <div className="col-sm-6 col-md-3">
                        <div className="form-group">
                            <label className="control-label label-sm" htmlFor="roof">Кровля</label>
                            <select
                                name="roof" id="roof"
                                className="form-control input-sm"
                                value={roof}
                                onChange={e => setRoof(e.target.value)}
                            >{roofOptions}</select>
                        </div>
                    </div>

                    <div className="col-sm-6 col-md-3">
                        <div className="form-group">
                            <label className="control-label label-sm" htmlFor="price">Макс цена (руб.):</label>
                            <input
                                type="text" name="price" id="price"
                                className="form-control input-sm"
                                value={maxPrice}
                                onChange={validateAndSetMaxPrice}
                            />
                        </div>
                    </div>
                    <div className="col-12 text-right">
                    <Link to="/projects" className="btn btn-outline-info btn-sm project-search_btn">подобрать</Link>
                    </div>
                </div>
            </form>
        </div>
    )
}

SearchProjectsForm.propTypes = {
    roofsList: PropTypes.arrayOf(PropTypes.string).isRequired,
    wallsList: PropTypes.arrayOf(PropTypes.string).isRequired,
    basementsList: PropTypes.arrayOf(PropTypes.string).isRequired
}

export default withRouter(SearchProjectsForm);
