import React from 'react';
import PropTypes from 'prop-types';

// import toaster from 'toasted-notes';

// import { sendOrder } from '../../actions/ordersActions';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExclamationCircle, faPaperPlane, faSpinner } from '@fortawesome/free-solid-svg-icons';

import 'toasted-notes/src/styles.css';
import './OrderForm.css';

// import Image from '../../temp/images';

const OrderFormComponent = ({
  fields, handleSubmit, handleChange, alert, title, errors, pending
}) => {

  const submitBtnTitle = pending
    ? <React.Fragment><FontAwesomeIcon icon={ faSpinner } spin /> отправляем</React.Fragment>
    : <React.Fragment><FontAwesomeIcon icon={ faPaperPlane } /> отправить</React.Fragment>

  return (
    <div className="modal-body">
      <form className="order_form" onSubmit={handleSubmit}>
        <h3 className="order_form--title">{title}</h3>
        {alert && <div dangerouslySetInnerHTML={{ __html: alert }} className="alert order_form--alert"></div>}
        <div className="form-group">
          <label htmlFor="person" className="control-label">
            Ваше имя: {
              (errors && errors.person) && <small className="invalid-feedback">{errors.person}</small>
            }
          </label>
          <input
            name="person" type="text" className="form-control"
            value={fields.person} onChange={ handleChange }
          />
        </div>
        <div className="form-group">
          <label htmlFor="phone" className="control-label">
            Телефон: {
              (errors && errors.phone) && <small className="invalid-feedback">{errors.phone}</small>
            }
          </label>
          <input
            name="phone" type="text" className="form-control"
            value={fields.phone} onChange={ handleChange }
          />
        </div>
        <div className="form-group">
          <label htmlFor="comment" className="control-label">Комментарий:</label>
          <textarea
            name="comment" type="text" className="form-control" rows="3"
            value={fields.comment} onChange={ handleChange }
          ></textarea>
        </div>
        <div className="form-group form_tools">
          <p className="caution">
            <FontAwesomeIcon className="mr-2 caution--icon" icon={faExclamationCircle} />
            Нажимая кнопку "отправить" Вы даете согласие на обработку персональных данных
        </p>
          <button 
            className="btn-outline-info btn btn-sm order-submit-btn"
            disabled={ pending }
          >
            {submitBtnTitle}
          </button>
        </div>
      </form>
    </div>
  )
}


OrderFormComponent.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  alert: PropTypes.any,
  title: PropTypes.string.isRequired,
  fields: PropTypes.shape({
    person: PropTypes.string.isRequired,
    phone: PropTypes.string.isRequired,
    comment: PropTypes.string.isRequired
  }),
  errors: PropTypes.shape({
    person: PropTypes.string,
    phone: PropTypes.string
  }),
  pending: PropTypes.bool.isRequired,
}

OrderFormComponent.defaultProps = {
  pending: false
}


export default OrderFormComponent;
