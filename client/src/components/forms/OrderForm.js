import React from 'react';
import PropTypes from 'prop-types';

const OrderForm = ({
    order
}) => {

    const sendOrder = e => {
        e.preventDefault();

    }

  return (
    <form className="order_form" onSubmit={ sendOrder }>
        <div className="form-group">
            <label htmlFor="person" className="control-label">Ваше имя:</label>
            <input name="person" type="text" minLength="3" className="form-control" />            
        </div>
        <div className="form-group">
            <label htmlFor="phone" className="control-label">Ваш телефон:</label>
            <input name="phone" type="text" className="form-control" />            
        </div>
        <div className="form-gtoup form_control">
            <button className="btn btn-sm btn-outline-success">Отправить</button>
        </div>
    </form>
  )
}

OrderForm.propTypes = {
    order: PropTypes.objectOf.isRequired
}

OrderForm.defaultProps = {
    order: {}
}

export default OrderForm;
