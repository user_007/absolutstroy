import React, { Component, Fragment } from 'react';
import { Helmet } from 'react-helmet';
import Lightbox from 'react-images';
// import Lightbox, { Modal, ModalGateway } from 'react-images';

import { getGalleryItems, getSizeOfGallery } from '../../actions/photoActions';

import Gallery from '../utils/GalleryComponent';
import LoadMoreButton from '../utils/LoadMoreButton'

import BreadCrumbs from '../utils/BreadCrumbs';
import Loading from '../utils/Loading';

import './Gallery.css';



class GalleryPage extends Component {

    state = {
        pageLoading: true,
        dataLoading: false,
        currentImage: 1,
        lightboxIsOpen: false,
        images: [],
        limit: 12,
        itemsOverall: 0
    }

    async componentDidMount() {

        //load images with data here
        const [ galleryItemsResult, gallerySizeResult ] = await Promise.all([
            getGalleryItems({ limit: 12, skip: 0 }), getSizeOfGallery()
        ]);

        // if (!result.success && result.status >= 500) return this.props.history.push('/errors/server-error');

        this.setState({
            pageLoading: false,
            images: [...galleryItemsResult.photoes],
            itemsOverall: gallerySizeResult.photoesAmount
        })

    }

    loadMorePhotoes = async () => {

        this.setState({ dataLoading: true });

        const { itemsOverall, images } = this.state;

        if ( images.length === itemsOverall ) return this.setState({ dataLoading: false });


        const result = await getGalleryItems({ limit: 12, skip: images.length });

        if (result.success) this.setState({ dataLoading: false, images: [...this.state.images, ...result.photoes] });

    }

    openImageInLightbox = index => {
        this.setState({
            lightboxIsOpen: true,
            currentImage: index 
        })
    }

    handleClick = e => {
        e.preventDefault();

        const { dataset: { index } } = e.target;

        this.openImageInLightbox(Number(index))
    }
    


    render() {
        const {
            history: {
                location: { pathname }
            }
        } = this.props;

        if (this.state.pageLoading) 
            return <Fragment>
                <Helmet>
                    <title>Фототчет по строительству домов</title>
                    <meta name="description" content="фотографии строительства домов на разных этапах: заливка фундамента, монтаж стен и кровли, сдача дома под ключ" />
                </Helmet>
                <Loading />
            </Fragment>;

        const {
            lightboxIsOpen: isOpen,
            dataLoading,
            itemsOverall,
            currentImage,
            images
        } = this.state;


        return (
            <div className="page">
                <Helmet>
                    <title>Фототчет по строительству домов</title>
                    <meta name="description" content="фотографии строительства домов на разных этапах: заливка фундамента, монтаж стен и кровли, сдача дома под ключ" />
                </Helmet>
                <div className="container">
                    <BreadCrumbs path={pathname} />
                    <h2 className="page-title">Галерея</h2>
                    {/* <ModalGateway>
                        {isOpen ? (
                        <Modal onClose={() => this.setState({ lightboxIsOpen: false })}>
                            <Lightbox views={images.map(img => ({ src: img.src }))} />
                        </Modal>
                        ) : null}
                    </ModalGateway> */}
                    <Lightbox
                        images={images.map(img => ({ src: img.src }))}
                        isOpen={ isOpen }
                        onClose={ () => this.setState({ lightboxIsOpen: false }) }
                        currentImage={ currentImage }
                        onClickNext={ () => this.setState({ currentImage: currentImage + 1 }) }
                        onClickPrev={ () => this.setState({ currentImage: currentImage - 1 }) }
                    />
                    <div className="page__content">
                        <Gallery images={ images } onClick={this.handleClick} />
                        <LoadMoreButton
                            loading={ dataLoading }
                            showButtonCondition={ images.length !== itemsOverall }
                            onClick={ this.loadMorePhotoes }
                        />
                        {/* <div className="gallery">
                            
                            {
                                images.map(
                                    (path, index) => <div className="gallery--item" key={keyGen()}>
                                        <img                                              
                                            src={path} alt="gallery" className="gal-img"
                                            onClick={ () => this.openImageInLightbox(index) }
                                        />
                                    </div>
                                )
                            }
                            
                        </div> */}
                        {/* <button className="btn btn-link" onClick={() => this.setState({ lightboxIsOpen: true })}>открыть лайтбокс...</button> */}
                    </div>
                </div>
            </div>
        )
    }
}

export default GalleryPage;
