import React, { Component } from 'react'

class PageNotFound extends Component {
  render() {
    return (
      <div className="content">
        <div className="container">
          <div className="page-not-found">
            ОШИБКА 404 <br/> страница не найдена
          </div>
        </div>
      </div>
    )
  }
}

export default  PageNotFound;
