import React, { Component } from 'react';

import { withRightsCheck } from '../../hoc-helpers';

import { getListOfAllMaterials, removeMaterial, addNewMaterial, updateMaterial } from '../../../actions/materialActions';
import LoadinPage from '../../utils/Loading';
import Modal from '../../utils/Modal';
// import PropTypes from 'prop-types';
import { v4 as keyGen } from 'uuid';
import toast from 'toasted-notes';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSave, faSpinner, faTrash, faEdit } from '@fortawesome/free-solid-svg-icons';

import 'toasted-notes/src/styles.css';



class AdminMaterilasPage extends Component {
  state = {
    pageLoading: true,
    modalIsOpen: false,
    materials: [],
    openedProcessingRequestsForMaterials: [],
    materialFormData: {
      name: '',
      category: 'wall',
      price: 0,
      id: ''
    }
  }


  async componentDidMount() {
    const { success, materialsList: materials } = await getListOfAllMaterials();

    if (success) {
      this.setState({ pageLoading: false, materials })
    } else {
      this.props.history.push('/error/server-error')
    }
  }


  openMaterialForm = (e, materialData) => {
    e.preventDefault();

    const { materialFormData, openedProcessingRequestsForMaterials: reqIds } = this.state;
    const materialIsExist = Object.keys(materialData).length > 0;

    if (materialIsExist)
      return this.setState({
        materialFormData: {
          name: materialData.material,
          category: materialData.type,
          price: materialData.price,
          id: materialData._id
        },
        modalIsOpen: true,
        openedProcessingRequestsForMaterials: reqIds
      })


    this.setState({
      materialFormData: {
        ...materialFormData,
        id: 'new'
      },
      modalIsOpen: true,
      openedProcessingRequestsForMaterials: reqIds
    })

  }

  closeModal = () => {
    const {
      openedProcessingRequestsForMaterials: reqIds, materialFormData: { id: materialId }
    } = this.state;

    this.setState({
      // eslint-disable-next-line
      openedProcessingRequestsForMaterials: reqIds.filter(id => id != materialId),
      materialFormData: {
        name: '',
        category: 'wall',
        price: 0,
        id: ''
      },
      modalIsOpen: false
    })
  }

  handleFormFieldChange = e => {
    const { materialFormData } = this.state;

    this.setState({
      materialFormData: {
        ...materialFormData,
        [e.target.name]: e.target.value
      }
    })
  }


  handleSubmit = async e => {
    e.preventDefault();

    const { 
      openedProcessingRequestsForMaterials: reqIds,
      materialFormData: {
        id: materialId, name: material, category: type, price        
      },
      materials
    } = this.state;

    const isNewMaterial = materialId === 'new';
    const notificationOptions = { position: 'bottom-right', duration: 5000 };
    let message = {}, newListOfMaterials = []

    this.setState({
      openedProcessingRequestsForMaterials: [ ...reqIds, materialId ]
    })

    if (isNewMaterial) {
      const materialData = {
        material, type, price
      }

      //Adding new material
      const result = await addNewMaterial(materialData);

      //handle response from the server...
      if (!result.success) {

        if (result.status >= 500) return this.props.history.push('/error/server-error');

        message.text =`Не удалось добавить материал ${material}`;

      } else {

        newListOfMaterials =  [...materials, result.newMaterial];

        message.text = `Новый материал ${material} добавлен`;
      } 

    } else {
      const materialData = {
        id: materialId, material, type, price
      }

      //Editing existing material
      const result = await updateMaterial(materialData);

      if (!result.success) {

        if (result.status >= 500) return this.props.history.push('/error/server-error');

        message.text = `не удалось обновить данные материала ${material}`

      } else {

        newListOfMaterials = materials.map(
          mat => {
            if (mat._id === materialId) {

              return { ...mat, material, type, price }
            }              

            return mat;
          }
        );

        message.text = `Данные материала ${material} изменены`;

      }

    }

    toast.notify( message.text, notificationOptions );

    this.setState({
      materials: newListOfMaterials,
      // eslint-disable-next-line
      openedProcessingRequestsForMaterials: reqIds.filter(id => id != materialId),
      modalIsOpen: false,
      materialFormData: {
        name: '',
        category: 'wall',
        price: 0,
        id: ''
      }
    })   
  }


  removeMaterial = async (e, materialId) => {
    const { 
      openedProcessingRequestsForMaterials: reqIds,
      materials
    } = this.state;

    this.setState({
      openedProcessingRequestsForMaterials: [...reqIds, materialId]
    })

    const result = await removeMaterial(materialId);

    if (!result.success) {

      if (result.status >= 500) return this.props.history.push('/error/server-error');


      toast.notify(
        `Не получилось удалить материал`,
        {
          position: 'bottom-right',
          duration: 5000
        }
      );
      return console.log(`Error occured while removing material with ID ${materialId}`, result.data);
    }

    // eslint-disable-next-line
    const newMaterialList = materials.filter(mat => mat._id != materialId);
    // eslint-disable-next-line
    const newReqIds = reqIds.filter(id => id != materialId);

    this.setState({
      openedProcessingRequestsForMaterials: newReqIds,
      materials: newMaterialList
    });

    toast.notify(
      `Материал удален`,
      {
        position: 'bottom-right',
        duration: 5000
      }
    )
  }



  render() {
    const {
      pageLoading, materials, openedProcessingRequestsForMaterials: reqIds, modalIsOpen, materialFormData
    } = this.state;

    let category;

    if (pageLoading) return <LoadinPage />;


    const newMaterialBtn = reqIds.includes('new')
      ? <button
        className="btn btn-outline-info disabled"
        onClick={null} disabled
      >
        Подождите...
        </button>
      : <button
        className="btn btn-outline-info"
        onClick={e => this.openMaterialForm(e, {})}
      >
        Добавить материал
        </button>


    const materialsTableRows = materials.map((mat, index) => {
      const requestInProcess = reqIds.includes(mat._id);

      const editBtn = requestInProcess
        ? <button
          className="btn btn-outline-dark btn-sm disabled"
          disabled
          onClick={null}
        >
          <FontAwesomeIcon icon={faSpinner} spin />
        </button>
        : <button
          className="btn btn-outline-dark btn-sm"
          onClick={e => this.openMaterialForm(e, mat)}
        >
          <FontAwesomeIcon icon={faEdit} />
        </button>;

      const removeBtn = requestInProcess
        ? <button
          className="btn btn-outline-dark btn-sm disabled"
          disabled
          onClick={null}
        >
          <FontAwesomeIcon icon={faSpinner} spin />
        </button>
        : <button
          className="btn btn-outline-dark btn-sm"
          onClick={e => this.removeMaterial(e, mat._id)}
        >
          <FontAwesomeIcon icon={faTrash} />
        </button>;

      
      switch(mat.type.trim().toLowerCase()) {
        case 'wall':
          category = 'Стены';
          break;
        case 'roof':
          category = 'Крыша';
          break;
        case 'basement':
          category = 'Фундамент';
          break;
          default:
            category = 'Другая'
      }
      

      return (
        <tr key={keyGen()} className="table__row">
          <td className="text-center">{index + 1}</td>
          <td>{mat.material}</td>
          <td>{category}</td>
          <td className="text-center">{mat.price}</td>
          <td className="text-center">{editBtn} {removeBtn}</td>
        </tr>
      )
    });


    return (
      <div className="admin__content">
        <Modal
          isOpen={modalIsOpen}
          handleModalClose={this.closeModal}
        >
          <div className="container">

            <form onSubmit={this.handleSubmit} className="form-horizontal">

              <div className="form-group">
                <label htmlFor="name">Название материала:</label>
                <input
                  className="form-control" type="text" 
                  name="name" id="name"
                  value={ materialFormData.name }
                  onChange={ this.handleFormFieldChange }
                />
              </div>

              <div className="form-group">
                <label htmlFor="category">Тип материала:</label>
                <select
                  name="category" id="category"
                  className="form-control"
                  value={materialFormData.category}
                  onChange={ this.handleFormFieldChange }
                >
                  <option value="wall">Стены</option>
                  <option value="roof">Крыша</option>
                  <option value="basement">Фундамент</option>
                </select>
              </div>
              <div className="form-group">
                <label htmlFor="price">Цена за м<sup>2</sup>:</label>
                <input
                  className="form-control" type="text" 
                  name="price" id="price"
                  value={ materialFormData.price }
                  onChange={ this.handleFormFieldChange }
                />
              </div>
              <div className="form-tools text-right">
                <button className="btn-info btn btn-sm"><FontAwesomeIcon icon={faSave} /> сохранить</button>
              </div>
            </form>
          </div>

        </Modal>
        <div className="container">
          <h3 className="admin__header">Материалы <span>{newMaterialBtn}</span></h3>

          <div className="admin__content-section-wrapper section_white">
            <div className="materials_table">
              <table className="table table-responsive table-striped">
                <thead>
                  <tr className="table__row">
                    <th className="text-center">№</th>
                    <th>Название</th>
                    <th>Категория</th>
                    <th className="text-center">Цена, руб.</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {materialsTableRows}
                </tbody>
              </table>
            </div>
          </div>

        </div>
      </div>
    )
  }
}

export default withRightsCheck(AdminMaterilasPage);
