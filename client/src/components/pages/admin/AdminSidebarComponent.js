import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { slide as Menu } from 'react-burger-menu';

import { verySimpleCheckIsHandHeldDevice as isHandHeldDevice } from '../../../utils/commonFunctions';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelopeOpenText, faBuilding, faCubes, faImages } from '@fortawesome/free-solid-svg-icons';

import Image from '../../../temp/images';
import Loading from '../../utils/Loading';

class AdminSidebarComponent extends Component {
    state = {
        loading: true,
        isHandHeldDevice: false
    }


    async componentDidMount() {
        window.addEventListener('resize', this.setDeviceType)

        this.setState({
            loading: false,
            isHandHeldDevice: window.innerWidth < 768
        })
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.setDeviceType)
    }
    


    setDeviceType = () => {
        this.setState({
            isHandHeldDevice: isHandHeldDevice()
        })
    }


    render() {
        const { loading, isHandHeldDevice } = this.state;

        if (loading) return <Loading />;

        const menuContent = (
            <ul className="admin_menu">
                <li className="admin_menu__item menu_header">
                    <img src={Image.LOGO} alt="логотип" className="menu_header__img" />
                    <h2 className="menu_header__title">Админ панель</h2>
                    {/* <Link className="admin_menu__navlink" to="/"><FontAwesomeIcon className="admin_menu__icon" icon={ faBuilding } /> Проекты</Link> */}
                </li>
                {/* <li className="admin_menu__item">
                    <p className="admin_menu__info">
                        Вы зашли как {user.name}
                    </p>
                </li> */}
                <li className="admin_menu__item">
                    <Link className="admin_menu__navlink" to="/admin/projects"><FontAwesomeIcon className="admin_menu__icon" icon={faBuilding} /> Проекты</Link>
                </li>
                <li className="admin_menu__item">
                    <Link className="admin_menu__navlink" to="/admin/orders"><FontAwesomeIcon className="admin_menu__icon" icon={faEnvelopeOpenText} /> Заказы</Link>
                </li>
                <li className="admin_menu__item">
                    <Link className="admin_menu__navlink" to="/admin/materials"><FontAwesomeIcon className="admin_menu__icon" icon={faCubes} /> Материалы</Link>
                </li>
                <li className="admin_menu__item">
                    <Link className="admin_menu__navlink" to="/admin/gallery"><FontAwesomeIcon className="admin_menu__icon" icon={faImages} /> Галерея</Link>
                </li>
            </ul>
        )

        return isHandHeldDevice
            ? <div className="mob-menu"><Menu>{menuContent}</Menu></div>
            : <div className="admin_sidebar hh-hide">{menuContent}</div>
    }
}


export default AdminSidebarComponent;
