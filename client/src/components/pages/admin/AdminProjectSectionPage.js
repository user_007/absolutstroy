import React, { Component } from 'react';
import { EditorState, convertToRaw, ContentState  } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft  from 'html-to-draftjs';


import { withRightsCheck } from '../../hoc-helpers';

import Rangeslider from 'react-input-range';
import toaster from 'toasted-notes';

import _ from 'lodash';

import FileLoader from '../../utils/MultiFileLoader';
import LoadingDataOverlay from '../../utils/LoadingDataOverlay';

import { getListOfBasements, getListOfRoofs, getListOfWalls } from '../../../actions/materialActions';
import { saveNewProject, getProjectById, editProject } from '../../../actions/projectActions';

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import 'react-input-range/lib/css/index.css';
import './Admin.css';



class AdminProjectSectionPage extends Component {
    state = {
        meta: {
            isExist: false,
            projectFormDataLoading: true,
            dataProcessing: false
        },
        newProjectData: {
            title: '',
            category: 0,
            files: [],
            timeToBuild: 30,
            walls: 0,
            basement: 0,
            roof: 0,
            area: 60,
            floors: 1,
            height: 2.75,
            price: 100000
        },
        errors: {},
        formLists: {
            roofs: [],
            walls: [],
            basements: []
        },
        editorState: EditorState.createEmpty()
    }


    onChange = e => {
        const { newProjectData, errors } = this.state;
        const fieldName = e.target.name.split('_');
        const fieldValue = e.target.value;

        // const errorsList = _.filter(errors, (field) => field.title !== fieldName)

        this.setState({
            newProjectData: {
                ...newProjectData,
                [fieldName[0]]: fieldValue
            },
            errors:  _.omit(errors, [fieldName])
        })
    }

    onFileLoad = files => {
        const { newProjectData: data } = this.state;

        this.setState({
            newProjectData: {
                ...data,
                files: files
            }
        })
    }

    validateFormData = () => {
        let errors = {}
        const { title, price, height, floors, area, timeToBuild } = this.state.newProjectData

        if (title.length < 3) errors.title = 'длина названия от 3 букв';
        if (Number(price) < 100000) errors.price = 'слишком маленькая цена проекта';
        if (Number(height) < 2) errors.height = 'слишком низкая высота потолка';
        if (Number(floors) < 1 || Number(floors) > 5) errors.floors = 'проверьте кол-во этажей';
        if (Number(area) < 20) errors.area = 'маленькая площадь фундамента';
        if (Number(timeToBuild) < 7) errors.timeToBuild = 'проверьте время монтажа конструкции';

        return { valid: !Object.keys(errors).length, errors }
    }

    handleEditorChange = state => {
        this.setState({ editorState: state })
    }


    async componentDidMount() {
        const { formLists, ...projectFormState } = this.state;
        const { projectId } = this.props.match.params;

        let listOfRequests = [
            getListOfBasements(), getListOfRoofs(), getListOfWalls()
        ]

        if (projectId) listOfRequests.push(getProjectById(projectId));

        const [ basements, roofs, walls, projectRequest ] = await Promise.all(listOfRequests);


        formLists.basements = basements;
        formLists.roofs = roofs;
        formLists.walls = walls;


        let stateChanges = {
            ...projectFormState,
            meta: {
                projectFormDataLoading: false,
                dataProcessing: false,
            },
            formLists
        };


        if (projectRequest) {
            const { features, photoes, description, ...project } = projectRequest.project;

            const formattedPhotoObject = photoes.map( photo => {
                return { file: null, preview: photo }
            });

            const blocksFromHtml = htmlToDraft(description);
            const { contentBlocks, entityMap } = blocksFromHtml;
			const contentState = ContentState.createFromBlockArray(contentBlocks, entityMap);
			const editorState = EditorState.createWithContent(contentState);

            const projectData = {
                ...project,
                ...features,
                files: formattedPhotoObject,
                // eslint-disable-next-line
                category: _.findIndex(formLists.walls, el => el.material == project.category),
                // eslint-disable-next-line
                walls: _.findIndex(formLists.walls, el => el.material == features.walls),
                // eslint-disable-next-line
                basement: _.findIndex(formLists.basements, el => el.material == features.basement),
                // eslint-disable-next-line
                roof: _.findIndex(formLists.roofs, el => el.material == features.roof)
            }

            stateChanges = { ...stateChanges, newProjectData: projectData, editorState }
        }

        this.setState({ ...stateChanges })
    }


    onSubmit = async e => {
        e.preventDefault();

        const { projectId } = this.props.match.params;


        this.setState({
            meta: {
                projectFormDataLoading: false,
                dataProcessing: true
            }
        });

        const { valid, errors } = this.validateFormData();

        if (!valid) return this.setState({
            meta: {
                projectFormDataLoading: false,
                dataProcessing: false
            },
            errors: errors
        });

        const { files, category, walls, basement, roof, ...project } = this.state.newProjectData;
        const { walls: wallsList, basements: basementsList, roofs: roofsList } = this.state.formLists;

        project.oldFiles = [];

        if (files.length) {
            files.forEach((image, ind) => {
                if (!!image.file) {
                    project[`file${ind}`] = image.file;
                } else {
                    project.oldFiles.push(image.preview);                    
                }

                // project[`file${ind}`] = !!image.file ? image.file : image.preview
            })
        }

        project.category = wallsList[category].material;
        project.walls = wallsList[walls].material;
        project.basement = basementsList[basement].material;
        project.roof = roofsList[roof].material;
        project.description = this.getEditorMarkdown();

        const data = new FormData();

        _.forIn(_.omit(project, ['_id', '__v', 'slug']), (val, key) => {
            data.append(key, val)
        });

        //TODO: check if project is exist, and call save or update depends on projectExists value
        const result = !projectId ? await saveNewProject(data) : await editProject(projectId, data);

        let notification;

        if (!result.success) {

            if (result.status >= 500) {

                notification = `Сервер ответил с ошибкой... попробуйте отправить запрос еще раз позже`
                
            } else {

                this.setState({
                    meta: {
                        projectFormDataLoading: false,
                        dataProcessing: false
                    },
                    errors: result.data.errors
                })                
            }            
            
        } else {

            notification = `Проект "${project.title}" успешно сохранен!`;

            const stateProjectData = !!projectId
                ? {
                    newProjectData: { ...this.state.newProjectData }                    
                }
                : {
                    newProjectData: {
                        title: '',
                        category: 0,
                        files: [],
                        timeToBuild: 30,
                        walls: 0,
                        basement: 0,
                        roof: 0,
                        area: 60,
                        floors: 1,
                        height: 2.75,
                        price: 100000
                    }, 
                    editorState: EditorState.createEmpty()
                }

            this.setState({
                meta: {
                    projectFormDataLoading: false,
                    dataProcessing: false
                },
                errors: {},
                ...stateProjectData
            });

        }

        toaster.notify(notification, { duration: 5000, position: 'bottom-right' });

    }


    getEditorMarkdown = () => draftToHtml(convertToRaw (this.state.editorState.getCurrentContent()));




    render() {
        const { projectFormDataLoading, dataProcessing } = this.state.meta;
        if (projectFormDataLoading) {
            return (
                <div>Loading...</div>
            )
        }


        const { newProjectData: field, formLists: list, errors } = this.state;

        const wallsList = list.walls.map((wall, index) => <option value={index} key={wall.material}>{wall.material}</option>);
        const basementsList = list.basements.map((basement, index) => <option value={index} key={basement.material}>{basement.material}</option>);
        const roofsList = list.roofs.map((roof, index) => <option value={index} key={roof.material}>{roof.material}</option>);


        return (
            <div className="admin__content">
                {dataProcessing && <LoadingDataOverlay />}
                <div className="container">
                    <h3>Новый проект</h3>
                    <form onSubmit={this.onSubmit} className="project_form shadow-sm">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="form-group">
                                    <FileLoader
                                        name="files" id="files"
                                        files={field.files}
                                        handleChange={this.onFileLoad}
                                    />
                                </div>
                            </div>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label htmlFor="title" className="control-label">Имя проекта:</label>
                                    <input
                                        name="title" type="text" className="form-control"
                                        value={field.title}
                                        onChange={this.onChange}
                                    />
                                    {errors.title && <small className="form-error">{errors.title}</small>}
                                </div>
                                <div className="form-group">
                                    <label htmlFor="category" className="control-label">Категория:</label>
                                    <select
                                        name="category" type="text" className="form-control"
                                        value={field.category}
                                        onChange={this.onChange}
                                    >
                                        {wallsList}
                                    </select>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="walls" className="control-label">Материал стен:</label>
                                    <select
                                        name="walls" type="text" className="form-control"
                                        value={field.walls}
                                        onChange={this.onChange}
                                    >
                                        {wallsList}
                                    </select>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="basement" className="control-label">Материал фундамента:</label>
                                    <select
                                        name="basement" type="text" className="form-control"
                                        value={field.basement}
                                        onChange={this.onChange}
                                    >
                                        {basementsList}
                                    </select>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="roof" className="control-label">Материал кровли:</label>
                                    <select
                                        name="roof" type="text" className="form-control"
                                        value={field.roof}
                                        onChange={this.onChange}
                                    >
                                        {roofsList}
                                    </select>
                                </div>

                            </div>



                        </div>
                        <div className="row">
                            <div className="col-md-3">
                                <div className="form-group">
                                    <label htmlFor="timeToBuild" className="control-label">Время изготовления:</label>
                                    <input
                                        name="timeToBuild" type="text" className="form-control"
                                        value={field.timeToBuild}
                                        onChange={this.onChange}
                                    />
                                    {errors.timeToBuild && <small className="form-error">{errors.timeToBuild}</small>}
                                </div>
                            </div>
                            <div className="col-md-3">
                                <div className="form-group">
                                    <label htmlFor="area" className="control-label">Площадь фундамента:</label>
                                    <input
                                        name="area" type="text" className="form-control"
                                        value={field.area}
                                        onChange={this.onChange}
                                    />
                                    {errors.area && <small className="form-error">{errors.area}</small>}
                                </div>
                            </div>
                            <div className="col-md-3">
                                <div className="form-group">
                                    <label htmlFor="height" className="control-label">Высота потолков:</label>
                                    <div className="slider-horizontal rangeslider">
                                        <Rangeslider
                                            orientation='horizontal'
                                            minValue={2.2} maxValue={3.5} step={0.05}
                                            name="height" type="text"
                                            className="form-control"
                                            value={field.height}
                                            handleLabel={field.height.toString()}
                                            formatLabel={val => `${val.toFixed(2)} м`}
                                            onChange={val => this.setState({ newProjectData: { ...this.state.newProjectData, height: val } })}
                                        />
                                    </div>
                                    {errors.height && <small className="form-error">{errors.height}</small>}
                                </div>
                            </div>
                            <div className="col-md-3">
                                <div className="form-group">
                                    <label htmlFor="floors" className="control-label">Количество этажей:</label>
                                    <div className="slider-horizontal rangeslider">
                                        <Rangeslider
                                            orientation='horizontal'
                                            minValue={1} maxValue={4} step={1}
                                            name="height" type="text"
                                            className="form-control"
                                            value={field.floors}
                                            handleLabel={field.floors.toString()}
                                            onChange={val => this.setState({ newProjectData: { ...this.state.newProjectData, floors: val } })}
                                        />
                                    </div>
                                    {errors.floors && <small className="form-error">{errors.floors}</small>}
                                </div>
                            </div>
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label htmlFor="description" className="control-label">Описание:</label>
                                    {/* <textarea
                                        name="description" rows="5" className="form-control"
                                        value={field.description}
                                        onChange={this.onChange}
                                    >
                                    </textarea> */}
                                    <Editor
                                        name="description" rows="5" className="form-control"
                                        placeholder="описание проекта..."
                                        editorState={this.state.editorState}
                                        onEditorStateChange={state => this.handleEditorChange(state) }
                                        localization={{
                                            locale: 'ru'                                            
                                        }}
                                    />
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label htmlFor="price" className="control-label">Цена проекта:</label>
                                    <input
                                        name="price" type="text" className="form-control"
                                        value={field.price}
                                        onChange={this.onChange}
                                    />
                                    {errors.price && <small className="form-error">{errors.price}</small>}
                                </div>
                            </div>
                            <div className="col-sm-6 align-self-end">
                                <div className="form-group">
                                    <button
                                        className="btn btn-success btn-block"
                                        disabled={!_.isEmpty(errors)}
                                    >{dataProcessing ? <span>Сохранение...</span> : <span>Сохранить</span>}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

export default withRightsCheck(AdminProjectSectionPage);
