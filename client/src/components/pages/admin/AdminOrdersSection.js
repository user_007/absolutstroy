import React, { Component } from 'react';

import { withRightsCheck } from '../../hoc-helpers';

import Loading from '../../utils/Loading';
import { v4 as keyGen } from 'uuid';

import OrderRow from '../../cards/AdminOrderTableRow';

import { getListOfOrders } from '../../../actions/ordersActions';

class AdminOrdersSection extends Component {

  state = {
    orders: [],
    pageLoading: true
  }

  async componentDidMount() {
    
    const result = await getListOfOrders();

    if (!result.success) {

      if (result.status >= 500) return this.props.history.push('/error/server-error');

      return console.log(result);

    }

    this.setState({
      pageLoading: false,
      orders: result.ordersList
    })

  }


  handleOpenDetails = index => {
    console.log('This func will try to open details of next order: ', this.state.orders[index]);
  }


  handleRemove = id => {
    
  } 
  

  render() {
    const { orders, pageLoading } = this.state;

    if (pageLoading) return <Loading />;

    console.dir(this.state);
    const orderRows = orders.map(
        (order, index) => <OrderRow
                            key={ keyGen() }
                            onOpenDetails={ this.handleOpenDetails }
                            handleRemove={ this.handleRemove }
                            index={ index } order={ order }
                            loading={ false }
                          />
    );

    return (
      <div className="admin__content">
        <div className="container">
          <h3 className="admin__header">Заказы с сайта</h3>
          {orderRows}
        </div>
      </div>
    )
  }
}


export default withRightsCheck(AdminOrdersSection);
