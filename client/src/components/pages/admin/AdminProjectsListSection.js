import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { withRightsCheck } from '../../hoc-helpers';

import { v4 as keyGen } from 'uuid';

import toaster from 'toasted-notes';

import ProjectRow from '../../cards/AdminProjectTableRow';
import Loading from '../../utils/Loading';

import { getListOfProjects, deleteProject } from '../../../actions/projectActions';

import './Admin.css';

class AdminProjectsListSection extends Component {
  static propTypes = {
    prop: PropTypes.any
  }

  static defaultProps = {

  }

  state = {
    projects: [],
    pageLoading: true,
    processingRequestsOfProjects: []
  }


  async componentDidMount() {
    const data = await getListOfProjects();

    this.setState({
      pageLoading: false,
      projects: data.projectsList
    });
  }



  handleRemoveProject = async id => {

    const notificationOpts = {
      duration: 5000,
      position: 'bottom-right'
    }

    this.setState({ 
      processingRequestsOfProjects: [...this.state.processingRequestsOfProjects, id] 
    })

    const result = await deleteProject(id);

    let notificationMessage;

    if(!result.success) {

      console.log('Произошла ошибка при удалении проекта: ', result)
      notificationMessage = `Произошла ошибка при удалении проекта: ${result.data}`;

    }

    notificationMessage = `Проект успешно удален!`;

    toaster.notify(notificationMessage, notificationOpts);

    this.setState({
      projects: this.state.projects.filter( pro => pro._id !== id ),
      processingRequestsOfProjects: this.state.processingRequestsOfProjects.filter(projectId => id !== projectId )
    });
  }


  handleEditProject = id => {
    const project = { field: 'one', value: 'two', row: 'three' };

    this.props.history.push(`/admin/projects/${id}/edit`, project);
  }



  render() {
    const { pageLoading, projects, processingRequestsOfProjects: reqIds } = this.state;

    if (pageLoading) return <Loading />

    const projectRows = projects.map(
      (project, index) => 
      <ProjectRow
        key={ keyGen() }
        loading={ reqIds.includes(project._id) }
        index={ index + 1 }
        project={ project }
        handleRemove={ this.handleRemoveProject }
        handleEdit={ this.handleEditProject }
      />
    )

    return (
      <div className="admin__content">
        <div className="container">
          <h3 className="admin__header">Список проектов <span><Link to="/admin/projects/new" className="btn btn-outline-info">Добавить проект</Link></span></h3>
          <div className="project-list-layout">
            { projectRows }
          </div>
        </div>
      </div>
    )
  }
}

export default withRightsCheck(AdminProjectsListSection);
