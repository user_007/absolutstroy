import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

// import _ from 'lodash';

import { userAuthorization } from '../../../actions/authActions';

import Image from '../../../temp/images';

import './Login.css';



class LoginPage extends Component {

  state = {
    redirect: false,
    errors: {},
    login: '',
    password: ''
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleSubmit = async e => {
    e.preventDefault();

    const { errors, redirect, ...credentials } = this.state;

    const result = await userAuthorization(credentials);

    if (!result.success) return this.setState({ errors: result.errors });

    this.setState({ redirect: true });
  }


  render() {
    const { errors, redirect, password, login } = this.state;
    let information;

    if (redirect) return <Redirect to="/admin" />

    if (Object.keys(errors).length) {
      information = <div className="login-form--info alert alert-danger"> { errors } </div> 
    } else {
      information = <div className="login-form--info alert alert-info">
          <p className="m-0">Для того, чтобы войти в админ панель, у Вас должен быть аккаунт с правами администратора.</p>
        </div> 
    }

    return (
      <div className="login-layout">
        <form className="login-form" onSubmit={this.handleSubmit}>
          {/* { 
              errors && 
              <div className="alert">
                { errors }
              </div> 
            } */}
          <div className="login-form--header">
            <img src={ Image.LOGO } alt="абсолют строй" className="login-form--logo" />
            <h2 className="login-form--title">ПАНЕЛЬ АДМИНИСТРАТОРА</h2>            
          </div>
          { information }
          <div className="form-group">
            <label htmlFor="login" className="control-label">Логин</label>
            <input
              type="text" className="form-control" name="login"
              value={login}
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group">
            <label htmlFor="password" className="control-label">Пароль</label>
            <input
              type="password" className="form-control" name="password"
              value={password}
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group mt-4 text-right">
            <button className="btn btn-outline-info" style={{ width: '40%', padding: '5px 10px' }}>Войти</button>
          </div>
        </form>
      </div>
    )
  }
}

export default LoginPage;
