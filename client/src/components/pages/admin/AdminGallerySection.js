import React, { Component } from 'react';

import { withRightsCheck } from '../../hoc-helpers';

import Modal from '../../utils/Modal';
import { v4 as keyGen } from 'uuid';
import toaster from 'toasted-notes';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSave, faSpinner, faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';

import FileLoader from '../../utils/MultiFileLoader'
import Loading from '../../utils/Loading';

import { getGalleryItems, addNewPhoto, removePhoto } from '../../../actions/photoActions';

import './Admin.css';

class AdminGallerySection extends Component {

  state = {
    photoes: [],
    itemsForRemove: [],
    pageLoading: true,
    modalIsOpen: false,
    processingRequest: [],
    newFile: {},
    comment: '',
    title: ''
  }


  async componentDidMount() {
    // get list of photoes
    const result = await getGalleryItems();


    // add list of photoes to the component State
    this.setState({
      pageLoading: false,
      photoes: result.photoes || [...this.state.photoes]
    });

    if (!result.success) 
      toaster.notify('Не удалось загрузить галерею...', { duration: 5000, position: 'bottom-right' });
  }

  handleRemovePhoto = async () => {

    if (this.state.processingRequest.includes('delete')) return;

    const { processingRequest, photoes, itemsForRemove } = this.state;

    this.setState({ processingRequest: [...processingRequest, 'delete'] });

    const result = await removePhoto(itemsForRemove);

    let notification;
    let stateChanges = {
      processingRequest: this.state.processingRequest.filter(req => req !== 'delete')
    }

    if (!result.success) {

      notification = 'не удалось удалить выбранные файлы';

    } else {

      notification = 'выбранные файлы успешно удалены';

      stateChanges.photoes = photoes.filter(photo => !itemsForRemove.includes(photo._id) );
      stateChanges.itemsForRemove = []

    }

    this.setState({ ...stateChanges });

    toaster.notify(notification, { duration: 5000, position: 'bottom-right' });
  }

  handleEditPhoto = id => {


  }

  onFileLoad = file => {
    this.setState({ processingRequest: [...this.state.processingRequest, 'save'] });

    //add request to add new photo object to DB 
    //and receive photo to add it to state photoes array
    const newPhoto = { file: file[0].file, preview: file[0].preview };

    this.setState({ newFile: newPhoto, processingRequest: this.state.processingRequest.filter(req => req !== 'save') })

  }


  handleSubmit = async e => {
    e.preventDefault();

    if (this.state.processingRequest.includes('save')) return;

    const { photoes, newFile, comment, title, processingRequest } = this.state;

    this.setState({ processingRequest: [...processingRequest, 'save'] });

    const result = await addNewPhoto({ file: newFile.file, comment, title });


    let notification;

    if (!result.success) {

      notification = 'Не удалось сохранить фотографию в галерею';

    } else {

      notification = 'фотография добавлена в галерею!';

      //add photo got after saving new Photo object on server
      photoes.unshift(result.photo);
      this.setState({
        // add notification
        processingRequest: this.state.processingRequest.filter(req => req !== 'save'),
        photoes,
        modalIsOpen: false
      });

    }


    toaster.notify(notification, { duration: 5000, position: 'bottom-right' });
  }


  openNewPhotoForm = e => {
    e.preventDefault();

    this.setState({ modalIsOpen: true })
  }


  handleAddOrRemoveFromDelList = id => {
    const { itemsForRemove } = this.state;

    const newItems = itemsForRemove.includes(id) 
      ?  itemsForRemove.filter( itemId => itemId !== id )
      : [...itemsForRemove, id];

    this.setState({ itemsForRemove: newItems });
  }



  render() {

    const {
      pageLoading, photoes, modalIsOpen, processingRequest, newFile, comment, title, itemsForRemove
    } = this.state;

    if (pageLoading) return <Loading />;

    const delBtn = processingRequest.includes('delete') 
      ? <button 
          className="btn btn-sm btn-outline-danger"
          disabled={ !itemsForRemove.length || processingRequest.includes('delete') }
          onClick={ this.handleRemovePhoto }
        >
          <FontAwesomeIcon icon={faSpinner} spin /> удаление...
        </button>
      : <button 
          className="btn btn-sm btn-outline-danger"
          disabled={ !itemsForRemove.length || processingRequest.includes('delete') }
          onClick={ this.handleRemovePhoto }
        >
          <FontAwesomeIcon icon={faTrash} /> удалить
        </button>



    let pageContent = photoes.length > 0
      ? photoes.map(photo => (        
          <div className="col-md-2 col-sm-3" key={keyGen()}>
            <div className="gallery--img-container">              
              <button className="gallery--img-edit-btn btn btn-outline-info">
                <FontAwesomeIcon icon={faEdit} />
              </button>
              <div className="form-check gallery--img-del-btn">
                <input                   
                  className="form-check-input" type="checkbox" 
                  checked={itemsForRemove.includes(photo._id)}
                  onChange={ e => this.handleAddOrRemoveFromDelList(photo._id) }
                />
              </div>
              <img 
                className="gallery--img" 
                src={photo.src} alt={photo.title || "фотография из галереи"} 
              />
            </div>            
          </div>
        ))
      : <div className="no-items-alert">
          <p className="m-0">Этот раздел пока пуст...</p>
        </div>;


    const newPhotoBtn = processingRequest.includes('save')
      ? <button
        className="btn btn-sm btn-outline-info disabled"
        onClick={null} disabled
      >
        Подождите...
        </button>
      : <button
        className="btn btn-sm btn-outline-info"
        onClick={this.openNewPhotoForm}
      >
        Добавить фото
        </button>

    const saveBtn = processingRequest.includes('save')
      ? <button className="btn-info btn disabled" disabled><FontAwesomeIcon icon={faSpinner} spin /> сохранение</button>
      : <button className="btn-info btn"><FontAwesomeIcon icon={faSave} /> сохранить</button>



    return (
      <div className="admin__content">
        <Modal
          isOpen={modalIsOpen}
          handleModalClose={() => this.setState({ modalIsOpen: false })}
        >
          <div className="photo_modal">
            <h3 className="photo_modal--title">Добавление фото</h3>
            <span className="alert alert_default">Для нормального отображения фото, лучше отредактировать заранее его по следующим параметрам: соотношение сторон 4х3, максимальный размер 1200px на 900px</span>
            <form onSubmit={this.handleSubmit} className="form-horizontal">

              <div className="form-group">
                <FileLoader
                  disabled={processingRequest.includes('save')}
                  multiple={false}
                  name="files" id="files"
                  handleChange={this.onFileLoad}
                  files={Object.keys(newFile).length ? [newFile] : []}
                />
              </div>

              <div className="form-group">
                <label htmlFor="title" className="control-label">Название</label>
                <input
                  name="title" id="title" className="form-control"
                  value={title}
                  onChange={e => this.setState({ title: e.target.value })}
                />
              </div>

              <div className="form-group">
                <label htmlFor="comment" className="control-label">Комментарий</label>
                <textarea
                  name="comment" id="comment" rows="4" className="form-control"
                  value={comment}
                  onChange={e => this.setState({ comment: e.target.value })}
                ></textarea>
              </div>
              <div className="form-tools">{saveBtn}</div>
            </form>
          </div>

        </Modal>
        <div className="container">
          <h3 className="admin__header">Галерея <span>{delBtn} {newPhotoBtn}</span></h3>
        </div>
        <div className="admin__content-section-wrapper">
          <div className="alert alert-info">
            <p className="text-sm m-0">Редактирование пока не работает! Выберите элементы галереи для удаления, поставив галочку в правом верхнем углу. Кнопка удаления не будет активна, пока не будет выбран хотя бы 1 картинка.</p>
          </div>
          <div className="row">{pageContent}</div>
        </div>
      </div>
    )
  }
}

export default withRightsCheck(AdminGallerySection);
