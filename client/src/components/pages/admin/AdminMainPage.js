import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { withRightsCheck } from '../../hoc-helpers';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelopeOpenText, faBuilding, faCubes, faImages } from '@fortawesome/free-solid-svg-icons';

class AdminMainPage extends Component {

  async componentDidMount() {

  }


  render() {
    return (
      <div className="admin__content">
        <div className="admin__links-wrapper">
          <div className="admin__link-block">            
            <Link to="/admin/projects" className="admin__link-title"><FontAwesomeIcon className="link-blovk__icon" icon={ faBuilding } /> Проекты</Link>
            <p className="link-block__description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores, nam.</p>
          </div>
          <div className="admin__link-block">            
            <Link to="/admin/orders" className="admin__link-title"><FontAwesomeIcon className="link-blovk__icon" icon={ faEnvelopeOpenText } /> Заказы</Link>
            <p className="link-block__description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores, nam.</p>
          </div>
          <div className="admin__link-block">            
            <Link to="/admin/materials" className="admin__link-title"><FontAwesomeIcon className="link-blovk__icon" icon={ faCubes } /> Материалы</Link>
            <p className="link-block__description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores, nam.</p>
          </div>
          <div className="admin__link-block">            
            <Link to="/admin/gallery" className="admin__link-title"><FontAwesomeIcon className="link-blovk__icon" icon={ faImages } /> Галерея</Link>
            <p className="link-block__description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores, nam.</p>
          </div>
        </div>
      </div>
    )
  }
}

export default withRightsCheck(AdminMainPage);
