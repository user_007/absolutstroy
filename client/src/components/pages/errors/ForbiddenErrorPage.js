import React from 'react';
import { Link } from 'react-router-dom';

import './Errors.css';

const ForbiddenErrorPage = () => {
  return (
    <div className="error-page">
      <div className="error-page__pane">
        <h1 className="error-page__code">403</h1>
        <h2 className="error-page__title">Ошибка сервера</h2>
        <p>Вы запросили страницу, доступ к которой ограничен специальными правами. Возможно, это закрытый раздел сайта или личные данные пользователя.</p>
        <p>Перейти на <Link to="/admin/login" className="error-page__link">страницу авторизации</Link></p>
        <p>Вернуться на <Link to="/" className="error-page__link">главную страницу</Link></p>
      </div>
    </div>
  )
}

export default ForbiddenErrorPage;