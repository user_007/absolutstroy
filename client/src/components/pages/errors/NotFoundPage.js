import React from 'react';
import { Link } from 'react-router-dom';

import './Errors.css';

const NotFoundPage = () => {
  return (
    <div className="error-page">
      <div className="error-page__pane">
        <h1 className="error-page__code">404</h1>
        <h2 className="error-page__title">Страница не найдена</h2>
        <p>Страница, которую Вы ищете не найдена. Возможно она была удалена или Вы ввели неверный адрес. Пожалуйста, проверьте адрес и попробуйте ввести его еще раз.</p>
        <p>Или попробуйте вернуться на <Link to="/" className="error-page__link">главную страницу</Link></p>
      </div>
    </div>
  )
}

export default NotFoundPage;
