import React from 'react';
import { Link } from 'react-router-dom';

import './Errors.css';

const ServerErrorPage = () => {
  return (
    <div className="error-page">
      <div className="error-page__pane">
        <h1 className="error-page__code">500</h1>
        <h2 className="error-page__title">Ошибка сервера</h2>
        <p>На сервере произошла непредвиденная ошибка. Пожалуйста, подождите, она будет исправлена в кратчайшие сроки.</p>
        <p>Попробуйте вернуться на <Link to="/" className="error-page__link">главную страницу</Link></p>
      </div>
    </div>
  )
}

export default ServerErrorPage;
