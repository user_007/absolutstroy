import React, { Component, Fragment } from 'react';
import { Helmet } from 'react-helmet';
import _ from 'lodash';
import toaster from 'toasted-notes';

import Slider from '../sliders/ProjectSlideComponent';
import { Redirect } from 'react-router-dom';
import Modal from '../utils/Modal';
import OrderForm from '../forms/OrderFormComponent';

import { getProjectData } from '../../actions/projectActions';
import { sendOrder } from '../../actions/ordersActions';

import Loading from '../utils/Loading';
import BreadCrumbs from '../utils/BreadCrumbs';

import './ProjectPage.css';

class ProjectPageComponent extends Component {

  state = {
    meta: {
      redirect: false,
      pageLoading: true,
      submitingOrder: false,
      show: false
    },
    project: {},
    order: {
      person: '',
      phone: '',
      comment: ''
    },
    errors: {}
  }

  async componentDidMount() {
    // TODO: get data from DB here
    const slug = this.props.match.params.slug;

    const result = await getProjectData(slug);

    if (!result.success) {
      const { history } = this.props;

      return result.status >= 500 ? history.push('/errors/server-error') : history.push('/errors/page-not-found');
    }

    this.setState({
      meta: {
        show: false,
        redirect: false,
        pageLoading: false
      },
      project: result.project
    })
  }


  createOrder = () => ({
    price: this.state.project.price,
    type: 'project',
    title: this.state.project.title,
    url: this.props.location.pathname
  })

  openDialog = () => {
    this.setState({
      meta: {
        ...this.state.meta,
        show: true
      }
    })
  }

  handleModalClose = () => {
    this.setState({
      meta: {
        ...this.state.meta,
        show: false
      }
    })
  }


  handleOrderFieldsChange = e => {

    const { order, errors } = this.state;

    this.setState({
      order: {
        ...order,
        [e.target.name]: e.target.value
      },
      errors: _.omit(errors, [e.target.name])
    })

  }


  handleSubmitOrderForm = async e => {
    e.preventDefault();
    const { project, meta, order } = this.state;

    this.setState({
      meta: {
        ...meta,
        submitingOrder: true
      }
    })

    const url = window.location.href;

    const details = `<b>Детали проекта</b><br />
    название: ${project.title}, страница проекта <a href="${url}">${url}</a>`

    const result = await sendOrder({ type: 'project', ...order, details });

    let notification;

    if (!result.success) {
      const { errors } = result.data;

      if ( errors && ( errors.person || errors.phone ) )
        return this.setState({
          meta: {
            ...this.state.meta,
            submitingOrder: false
          },
          errors: errors
        });

      notification = `не удалось отправить заявку на проект ${project.title}`;

    } else {

      notification = `Заявка на проект ${project.title} успешно отправлена. Мы свяжемся с Вами в ближайшее время.`

    }

    this.setState({
      meta: {
        ...this.state.meta,
        submitingOrder: false,
        show: false
      },
      order: {
        person: '',
        phone: '',
        comment: ''
      }
    });

    toaster.notify(notification, { duration: 5000, position: 'bottom-right' });

  }



  render() {
    const {
      project: { photoes, features: stats, ...project },
      meta: { redirect, pageLoading, show },
      order
    } = this.state;

    const {
      location: {
        pathname
      }
    } = this.props;


    if (redirect) return <Redirect to="/" />;
    if (pageLoading) 
      return <Fragment>
        <Helmet>
          <title>Проект дома</title>
          <meta name="description" content={`готовый проект дома, с описанием, характеристиками и расценками`} />
        </Helmet>
        <Loading />
      </Fragment>;

    let fromWallsMaterial = '';

    switch(stats.walls.toLowerCase()) {
      case 'сип панели':
        fromWallsMaterial = ' из сип панелей';
        break;
      case 'лстк':
        fromWallsMaterial = ' из металлоконструкций';
        break;
      default:
        fromWallsMaterial = '';
    }

    const currency = new Intl.NumberFormat(
      'ru-RU', { style: 'currency', currency: 'RUB'}
    );

    return (
      <div className="page project-page">
        <Helmet>
          <title>Проект дома из {stats.walls}</title>
          <meta name="description" content={`готовый проект ${stats.floors}-этажного дома${fromWallsMaterial} площадью ${stats.area} квадратных метров за ${project.price} руб.`} />
        </Helmet>
        <div className="container">
        <BreadCrumbs 
          path={ pathname } title={ `проект "${project.title}"` }
        />
        <h2 className="page-title">{project.title}</h2>
          <div className="project-data">
            <Modal 
             isOpen={ show } handleModalClose={ this.handleModalClose }
            >
                <OrderForm                    
                  fields={ order }
                  title={`Заказ проекта "${project.title}"`}
                  alert="Укажите Ваше имя и номер телефона, наши специалисты свяжутся с Вами в ближайшее время"
                  handleChange={ this.handleOrderFieldsChange }
                  handleSubmit={ this.handleSubmitOrderForm }
                />
            </Modal>
            <div className="row">
              <div className="col-md-6 py-3">
                {/* слайдер... */}
                <Slider images={photoes} />
              </div>

              <div className="col-md-6">
                {/* характеристики */}
                <div className="table-responsive project-table">
                  <h3 className="project-table__caption">Характеристики:</h3>
                  <table className="table table-sm table-striped project_table">
                    <tbody>
                      <tr className="project-table__row">
                        <th className="project-table__stat-name">материал стен</th>
                        <td className="project-table__stat-value">{stats.walls}</td>
                      </tr>
                      <tr className="project-table__row">
                        <th className="project-table__stat-name">материал крышли</th>
                        <td className="project-table__stat-value">{stats.roof}</td>
                      </tr>
                      <tr className="project-table__row">
                        <th className="project-table__stat-name">тип фундамента</th>
                        <td className="project-table__stat-value">{stats.basement}</td>
                      </tr>
                      <tr className="project-table__row">
                        <th className="project-table__stat-name">площадь фундамента</th>
                        <td className="project-table__stat-value">{stats.area} кв. м</td>
                      </tr>
                      <tr className="project-table__row">
                        <th className="project-table__stat-name">кол-во этажей</th>
                        <td className="project-table__stat-value">{stats.floors}</td>
                      </tr>
                      <tr className="project-table__row">
                        <th className="project-table__stat-name">высота потолков</th>
                        <td className="project-table__stat-value">{stats.height} м</td>
                      </tr>
                      <tr className="project-table__row">
                        <th className="project-table__stat-name">время изготовления и монтажа</th>
                        <td className="project-table__stat-value">{stats.timeToBuild} дней</td>
                      </tr>
                    </tbody>
                  </table>
                </div>

                <p className="price-remark alert">Цена проекта указана с учетом всех вышеописанных характеристик! Внесения изменений в проект можно обговорить у нас в офисе или по телефону.</p>

                <div className="price-block">
                  <button 
                    className="btn btn-action"
                    onClick={ this.openDialog }
                  >заказать</button>
                  <p className="m-0">стоимость: <span className="price">{currency.format(project.price)}</span></p>
                </div>
              </div>

            </div>

            <div className="row mt-5">
              <div className="col-sm-12">
                <div className="project__description">
                  <h3 className="section-title">Описание</h3>
                  {/* <p className="description-text">{project.description}</p> */}
                  <p dangerouslySetInnerHTML={{ __html: project.description }} />
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    )
  }
}

export default ProjectPageComponent;
