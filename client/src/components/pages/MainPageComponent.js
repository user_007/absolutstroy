import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import AOS from 'aos';
import { v4 as keyGen } from 'uuid';
import { Waypoint } from 'react-waypoint';

import SectionTitle from '../utils/SectionTitle';

import { ServiceSection, MapSection as Map, BrandsList, MainSliderSection } from '../static';
import { CategoryContainer, CategoryImageContainer } from '../sections';

import Slider from 'react-slick';
import Stats from '../utils/StatisticsComponent';
import Loading from '../utils/Loading';
import ProjectCard from '../cards/BuildingCard';
import Calculator from '../calc/CalcComponent';

import SearchProjectsForm from '../forms/SearchProjectsForm';

import Image from '../../temp/images';
import categories from '../../temp/main-page-categories';

import { getListOfProjects } from '../../actions/projectActions';
import { getListOfBasements, getListOfRoofs, getListOfWalls } from '../../actions/materialActions';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import 'aos/dist/aos.css';
import './MainPage.css';




class MainPageComponent extends Component {

  state = {
    meta: {
      dataLoading: true
    },
    projects: [],
    errors: {},
    formLists: {
      walls: [],
      roofs: [],
      basements: []
    }
  }

  countUp = React.createRef();
  calc = React.createRef();



  async componentDidMount() {
    const querySearchParams = new URLSearchParams();
    querySearchParams.append('limit', 6);


    AOS.init({
      once: true,
      offset: 50,
      duration: 1200,
      delay: 0
    });

    const [data, basements, roofs, walls] = await Promise.all([
      getListOfProjects(querySearchParams), getListOfBasements(), getListOfRoofs(), getListOfWalls()
    ])

    if (data.status && data.status >= 500) this.props.history.push('/error/server-error');

    this.setState({
      meta: {
        dataLoading: false
      },
      projects: data.projectsList,
      formLists: {
        basements, roofs, walls
      }
    })
  }



  render() {

    const {
      projects,
      meta: { dataLoading },
      formLists
    } = this.state;

    if (dataLoading)
      return <Fragment>
               <Helmet>
                 <title>Строительство домов под ключ</title>
                 <meta name="description" content="строительство домов из Сип панелей, металлоконструкций, Ракушечника, Газобетонных блоков под ключ по доступной цене в Севастополе" />
               </Helmet>
               <Loading />
             </Fragment>;


    const settings = {
      dots: true,
      infinite: true,
      speed: 700,
      autoplay: true,
      autoplaySpeed: 5000,
      centerMode: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 820,
          settings: {
            slidesToShow: 2,
            centerPadding: '10px'
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            centerPadding: '0px'
          }
        }
      ]
    };


    const projectSlidersContent = projects.map(project => <div className="project-slide" key={keyGen()}><ProjectCard {...project} /></div>);

    const categoriesImages = categories.images.map( img => <div key={keyGen()} className="col-md-4"><CategoryImageContainer { ...img } /></div> );

    const categoriesList = categories.data.map( cat => <CategoryContainer key={keyGen()} { ...cat } /> );   



    return (
      <div className="content">
        <Helmet>
          <title>Строительство домов под ключ</title>
          <meta name="description" content="строительство домов из Сип панелей, металлоконструкций, Ракушечника, Газобетонных блоков под ключ по доступной цене в Севастополе" />
        </Helmet>

        <MainSliderSection />

        <ServiceSection />

        <div className="section description_section">
          <div className="container">
            <div className="co-description">
              <SectionTitle title="Чем мы занимаемся" />
              {/* <h2 className="section__title">
                Чем мы занимаемся
            </h2> */}
              <p className="description py-4 m-0" data-aos="fade-up"> Компания абсолют строй 92 занимается проектированием и строительством домов, котеджей и промышленных помещений различного назначения под ключ в Севастополе, используя наиболее популярные и недорогие технологии: дома из Сип панелей, строительство зданий и объектов из металлокострукций, а также строительство домов из ракушечника и газобетона. </p>
              <div className="work-categories" data-aos="slide-up">

                <div className="work-category__images">
                  <div className="row">{ categoriesImages }</div>
                </div>

                { categoriesList }

              </div>
            </div>
          </div>
        </div>

        <div className="section blue_section">
          <div className="container">
            <h2 className="section__title work-step_title">Наши преимущества</h2>
            <div className="co-work">
              {/* инфографика */}

                  <div ref={this.countUp} className="infographics">

                    <Waypoint>
                      <div><Stats /></div>
                    </Waypoint>
                  </div>
                
            </div>
          </div>
        </div>

        <div className="section project_section">
          <div className="container">
            <div data-aos="flip-left"><SectionTitle title="Наши проекты" /> </div>                       
            {/* <h2 className="section__title" data-aos="flip-left">Наши проекты</h2> */}
            <p className="py-4">
              Для экономии Вашего времени, Вы можете выбрать готовый типовой проект строительства дома. Вы можете <Link style={{ color: "#006497", fontWeight: "bold" }} to="/projects">посмотреть все дома</Link> или указать, интересующие Вас критерии, если уже определились каким примерно должно быть Ваше жилье. Для любого проекта можно обсудить внесение изменений по Вашему усмотрению.
            </p>
            <div className="slider-wrapper" data-aos="fade-up">
              <SearchProjectsForm
                roofsList={formLists.roofs.map(roof => roof.material)}
                wallsList={formLists.walls.map(wall => wall.material)}
                basementsList={formLists.basements.map(base => base.material)}
              />

              <h3 className="section__sub-title">Последние добавленные проекты:</h3>
              <Slider {...settings} className="py-3 main-project-slider" >
                {projectSlidersContent}
              </Slider>

            </div>

          </div>
        </div>

        <div className="section blue_section">
          <img src={Image.CALC_BG} alt="строительство домов" className="bg-img" />
          <div ref={this.calc}><Calculator /></div>
        </div>

        <div className="section">
          <div className="container">
            <SectionTitle title="Материалы, которые мы используем" />
            {/* <h2 className="section__title">
              Материалы, которые мы используем
            </h2> */}
            <BrandsList />
          </div>
        </div>

        <div className="section section_map">
          <Map />
        </div>

      </div>
    )
  }
}

export default MainPageComponent;
