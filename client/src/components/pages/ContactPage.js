import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import BreadCrumbs from '../utils/BreadCrumbs';
import toast from 'toasted-notes';

import data from '../../temp/main-page-categories';

import _ from 'lodash';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faSpinner, faExclamationCircle, faPaperPlane, faClock, faMapMarkerAlt, faAddressBook
} from '@fortawesome/free-solid-svg-icons';

import { MapSection as Map } from '../static';

import './ContactPage.css';

import { sendOrder } from '../../actions/ordersActions';

class ContactPage extends Component {

  state = {
    person: '',
    phone: '',
    comment: '',
    sendingMessage: false,
    errors: {}
  }


  handleFeedbackFieldChange = e => {
    e.preventDefault();

    const fieldName = e.target.name;
    const value = e.target.value;



    if (!this.state.sendingMessage) this.setState({
      [fieldName]: value,
      errors: _.omit(this.state.errors, [fieldName])
    })
  }


  submitOrder = async e => {
    e.preventDefault();

    const { sendingMessage, errors, ...data } = this.state;

    if (sendingMessage) return;

    this.setState({ sendingMessage: true })

    let message = {};

    const result = await sendOrder(data);

    if (!result.success) {

      if (result.data.errors.person || result.data.errors.phone) 
        return this.setState({ errors: result.data.errors, sendingMessage: false })

      message.type = 'error';
      message.text = 'Не удалось отправить сообщение, попробуйте еще раз позже';

    } else {

      message.type = 'success';
      message.text = `${data.person}, Ваш запрос отправлен успешно. Мы с Вами свяжемся в ближайшее время`

    }

    toast.notify(
      message.text,
      {
        duration: 5000, position: 'bottom-right'
      }
    );

    this.setState({
      person: '',
      phone: '',
      comment: '',
      sendingMessage: false
    })

  }



  render() {
    const {
      history: {
        location: { pathname }
      }
    } = this.props;

    const {
      person, phone, comment, sendingMessage, errors
    } = this.state;

    const submitBtn = sendingMessage
      ? <button className="btn-outline-info btn btn-sm order-submit-btn disabled" disabled><FontAwesomeIcon className="mr-2" icon={faSpinner} spin /> Отправка...</button>
      : <button className="btn-outline-info btn btn-sm order-submit-btn"><FontAwesomeIcon className="mr-2" icon={faPaperPlane} /> Отправить</button>


    return (
      <div className="page">
        <Helmet>
          <title>Контактные данные строительной компании</title>
          <meta name="description" content="телефон, электронная почта и адрес офиса строительной компании Абсолют-Строй92" />
        </Helmet>
        <div className="container">
          <BreadCrumbs path={pathname} />
          <h2 className="page-title">Контактные данные</h2>
          <div className="page__content">
            <p className="alert alert_default">Вы можете связаться с нами любым удобным для Вас способом, описанным ниже. Заходите в наш офис в городе Севастополь или отправьте нам Ваше имя и телефон через форму обратной связи, и мы перезвоним Вам в кратчайшие сроки.</p>
            <div className="contact-data_section">
              <div className="row">
                <div className="col-md-5">
                  <div className="office-data">
                    <div className="office-data__block">
                      <h4 className="office-data__title"><FontAwesomeIcon icon={faMapMarkerAlt} /> Адрес офиса</h4>
                      <div className="office-data__details">
                        <span>Россия, г.Севастополь<br />ул. Бутакова 46/1</span>
                      </div>
                    </div>
                    <div className="office-data__block">
                      <h4 className="office-data__title"><FontAwesomeIcon icon={faAddressBook} /> Контакты</h4>
                      <div className="office-data__details">
                        <span>{ data.contacts.phone1 }</span>
                      </div>
                      <div className="office-data__details">
                        <span>{ data.contacts.phone2 }</span>
                      </div>
                      <div className="office-data__details">
                        <span>{ data.contacts.email }</span>
                      </div>
                    </div>
                    <div className="office-data__block">
                      <h4 className="office-data__title"><FontAwesomeIcon icon={faClock} /> Часы работы</h4>
                      <div className="office-data__details">
                        <span>пн-сб 9:00 - 18:00<br />выходной: воскресенье</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-6">
                  <form className="order_form" onSubmit={this.submitOrder}>
                    <h3 className="order_form--title">Форма обратной связи</h3>
                    <div className="form-group">
                      <label htmlFor="person" className="control-label">Ваше имя: { errors.person && <small className="invalid-feedback">{errors.person}</small> }</label>
                      <input
                        name="person" type="text" className="form-control"
                        value={person} disabled={sendingMessage}
                        onChange={this.handleFeedbackFieldChange}
                      />
                      
                    </div>
                    <div className="form-group">
                      <label htmlFor="phone" className="control-label">Телефон: { errors.phone && <small className="invalid-feedback">{errors.phone}</small> }</label>
                      <input
                        name="phone" type="text" className="form-control"
                        value={phone} disabled={sendingMessage}
                        onChange={this.handleFeedbackFieldChange}
                      />
                      
                    </div>
                    <div className="form-group">
                      <label htmlFor="comment" className="control-label">Комментарий:</label>
                      <textarea
                        name="comment" type="text" className="form-control" rows="3"
                        value={comment} disabled={sendingMessage}
                        onChange={this.handleFeedbackFieldChange}
                      ></textarea>
                    </div>
                    <div className="form-group form_tools">
                      <p className="caution">
                        <FontAwesomeIcon className="mr-2 caution--icon" icon={faExclamationCircle} />
                        Нажимая кнопку "отправить" Вы даете согласие на обработку персональных данных
                      </p>
                      {submitBtn}
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <div className="map_section mb-3">
              <div className="row">
                <div className="col-md-12">
                  <Map withTabs={false} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default ContactPage;
