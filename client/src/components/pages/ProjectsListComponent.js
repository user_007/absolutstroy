import React, { Component, Fragment } from 'react';
import ProjectFilters from '../forms/ProjectFiltersComponent';
import { Helmet } from 'react-helmet';
import _ from 'lodash';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSlidersH } from '@fortawesome/free-solid-svg-icons';

import { getListOfBasements, getListOfRoofs, getListOfWalls } from '../../actions/materialActions';
import { getListOfProjects } from '../../actions/projectActions';

import { verySimpleCheckIsHandHeldDevice as isHandHeldDevice } from '../../utils/commonFunctions'

import Loading from '../utils/Loading';
import BreadCrumbs from '../utils/BreadCrumbs';
import BuildingCard from '../cards/BuildingCard';
import Modal from '../utils/Modal';

import './ProjectsList.css';



class ProjectsListComponent extends Component {

  state = {
    meta: {
      loadingData: true,
      isHandHeldDevice: false
    },
    filtersShow: false,
    projects: [],
    filters: {
      price: {
        min: 0, max: 60000000
      },
      height: {
        min: 2, max: 3.50
      },
      floors: {
        min: 1, max: 4
      },
      walls: 0,
      roof: 0,
      basement: 0
    },
    limits: {
      price: {
        min: 0, max: 60000000
      },
      height: {
        min: 2, max: 3.50
      },
      floors: {
        min: 1, max: 4
      }
    },
    materials: {
      basements: [],
      roofs: [],
      walls: []
    }
  }

  projectsListFilters = React.createRef();

  triggerVisabilityOfProjectFilters = e => {
    e.preventDefault()
    const { filtersShow } = this.state;

    this.setState({
      filtersShow: !filtersShow
    })
  }


  async componentDidMount() {
    window.addEventListener('resize', this.setDeviceType);

    try {

      // let filters = {}, limits = {};
      
      const [ 
        projects, wallsList, roofsList, basementsList 
      ] = await Promise.all([
        getListOfProjects(), getListOfWalls(), getListOfRoofs(), getListOfBasements()
      ]);
  
      this.setState({
        meta: {
          isHandHeldDevice: isHandHeldDevice(),
          loadingData: false
        },
        projects: projects.projectsList,
        materials: {
          walls: [{material: 'Все типы', price: 0}, ...wallsList],
          roofs: [{material: 'Все типы', price: 0}, ...roofsList],
          basements: [{material: 'Все типы', price: 0}, ...basementsList]
        }
      })
  

    } catch (err) {

      console.log(err);
      
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.setDeviceType)
  }
  

  handleFiltersChange = (val, field) => {
    const { limits } = this.state;

    if (typeof val === 'object') {

      if (Object.keys(val).some(field => isNaN(val[field])))
        return console.log('Some of object values is not a number: ', val);

      if (val.min < limits[field].min) val.min = limits[field].min;
      if (val.max > limits[field].max) val.max = limits[field].max;
      
    }

    this.setState({
      filters: {
        ...this.state.filters,
        [field]: val
      }
    })
  }


  setDeviceType = () => {
    this.setState({ 
      meta: { 
        ...this.state.meta, 
        isHandHeldDevice: isHandHeldDevice() 
      } 
    })
  }
  

  handleSubmitFilters = async e => {
    e.preventDefault();
    this.setState({ 
      meta: { 
        ...this.state.meta,
        loadingData: true 
      } 
    });

    const { filters, materials: List } = this.state;
    // const formData = new FormData();
    const querySearchParams = new URLSearchParams();

    //TODO: Do this check in Submin function
    // if (val.min > val.max)

    _.forEach(filters, (val, key) => {

      if (typeof val === 'object') {
        const { min, max } = val;

        querySearchParams.append(`min_${key}`, min);
        querySearchParams.append(`max_${key}`, max);
      } else {
        const path = key.substring(key.length - 1) === 's' ? key : key + 's';

        querySearchParams.append(key, List[path][val].material);
      }
      
    });


    const result = await getListOfProjects(querySearchParams);

    this.setState({
      meta: {
        ...this.state.meta,
        loadingData: false
      },
      filtersShow: false,
      projects: result.projectsList
    })
  }


  handleCompleteChangeOfFilterValue = field => {
    // const val = Object.assign({}, this.state.filters[field]);

    //TODO:
    // if (val.min > val.max) {
    //   this.setState({
    //     filters: {
    //       ...this.state.filters,
    //       [field]: {
    //         min: val.min,
    //         max: val.min
    //       }
    //     }
    //   })
    // }
  }



  render() {
    const {
      projects, filters, limits, filtersShow,
      meta: { loadingData },
      materials: lists} = this.state;

    const {
      history: {
        location: { pathname }
      }
    } = this.props;

    if (loadingData) 
      return <Fragment>
        <Helmet>
          <title>Проекты домов из Сип панелей, ЛСТК, Ракушечника, Газобетона</title>
          <meta name="description" content="готовые проекты домов с размерами и ценой из сип панелей, металлоконструкций, ракушечника, газобетонных блоков" />
        </Helmet>
        <Loading />
      </Fragment>

    //TODO: empty list of projects instance
    const projectCards = _.isEmpty(projects) 
      ? <div className="no-content">Проектов не найдено</div>
      : projects.map(project => (
        <div className="col-lg-4 col-md-6 col-sm-6 project-card-wrapper" key={project._id}>
          <BuildingCard {...project} />
        </div>
      ))

    const filtersContent = <ProjectFilters
                            handleChange={ this.handleFiltersChange }
                            handleSubmit={ this.handleSubmitFilters }
                            handleFieldValidation={this.handleCompleteChangeOfFilterValue}
                            limits={ limits }
                            filters={ filters }
                            lists={ lists }
                          />


    const filtersBlock = this.state.meta.isHandHeldDevice
      ? <Fragment>
          <button
            className="filters-show-btn btn"
            onClick={ this.triggerVisabilityOfProjectFilters }
          ><FontAwesomeIcon icon={faSlidersH} /></button>
          <Modal 
            isOpen={ filtersShow } 
            handleModalClose={ () => this.setState({ filtersShow: false }) }
          >
          <div className="mobile-filters">
            {filtersContent}
          </div>
          </Modal>
        </Fragment> 
      : <div className="col-md-3">{filtersContent}</div>

    

    return (
      <div className="page">
        <Helmet>
          <title>Проекты домов из Сип панелей, ЛСТК, Ракушечника, Газобетона</title>
          <meta name="description" content="готовые проекты домов с размерами и ценой из сип панелей, металлоконструкций, ракушечника, газобетонных блоков" />
        </Helmet>
        <div className="container">
          <BreadCrumbs path={pathname} />
          <h2 className="page-title">Список проектов</h2>
          <div className="row">
            <div className="col-md-9">
              <div className="row">
                {projectCards}
              </div>
            </div>
            {filtersBlock}
          </div>
        </div>
      </div>
    )
  }
}

export default ProjectsListComponent;
