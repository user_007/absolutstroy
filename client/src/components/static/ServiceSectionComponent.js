import React from 'react';

import { faDraftingCompass, faHome, faPaintRoller } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const  ServiceSectionComponent = () => {
    return (
        <div className="section service_section">
          <div className="container">
            <div className="row">
              <div className="col-md-4">
                <div className="service">
                  <div className="service__icon"><FontAwesomeIcon icon={faDraftingCompass} /></div>
                  <div className="srvice__info">
                    <div className="service__title">Проектирование</div>
                    <div className="service__description">Делаем проекты строительства жилых домов, коттеджей и промышленных помещений</div>
                  </div>
                </div>
              </div>
              <div className="col-md-4">
                <div className="service">
                  <div className="service__icon"><FontAwesomeIcon icon={faHome} /></div>
                  <div className="srvice__info">
                    <div className="service__title">Строительство</div>
                    <div className="service__description">Занимаемся строительством домов, коттеджей и промышленных помещений под ключ</div>
                  </div>
                </div>
              </div>
              <div className="col-md-4">
                <div className="service">
                  <div className="service__icon"><FontAwesomeIcon icon={faPaintRoller} /></div>
                  <div className="srvice__info">
                    <div className="service__title">Прочие услуги</div>
                    <div className="service__description">Монтаж водосточной системы, ремонт и отделка фасадов, кровельные работы</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    )
}


export default ServiceSectionComponent;
