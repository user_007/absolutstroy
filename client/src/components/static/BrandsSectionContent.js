import React from 'react';
import Brand from '../utils/BrandComponent';
import { v4 as keyGen } from 'uuid';
import Image from '../../temp/images';


const BrandsSectionContent = () => {

    const brandsList = [
        Image.BRAND_DOCKE, Image.BRAND_KAPAROL, Image.BRAND_TEKKURILA, Image.BRAND_SHINGLAS, Image.BRAND_ROCKWOOL,
        Image.BRAND_TEKNONIKOL, Image.BRAND_STARATELI, Image.BRAND_URSA, Image.BRAND_CERESIT, Image.BRAND_KNAUF
      ].map(
        brand => <div key={keyGen()} className="col-6 col-sm-4 col-md-3" data-aos="flip-down"><Brand image={brand} /></div>
      )


    return (
        <div className="row justify-content-around flex-wrap align-items-center py-4">
              {brandsList}
            </div>
    )
}

export default BrandsSectionContent;
