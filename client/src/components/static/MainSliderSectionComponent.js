import React from 'react';
import { Link } from 'react-router-dom';

import Image from '../../temp/images';

const MainSliderSectionComponent = () => {
    
    return (
        <div className="section slider_section">
          <div className="main-slider">
            <div className="slide">
              <img src={Image.HOUSE} alt="house" className="slide__img" />
              <div className="slide-overlay">
                <div className="container h-100">
                  <div className="slide__info">
                    <div className="container">
                      <div className="info-container" data-aos="fade-right">
                        <h2 className="slide__title">Строим красивые теплые дома</h2>
                        <p className="slide__text">Разработаем для Вас проект надежного, комфортного дома любой сложности, а также реализуем строительство жилья в кратчайшие сроки по всем ГОСТам.</p>
                        <div className="slide__tools">
                          <Link to="/projects" className="btn btn-outline-light btn-sm">Подробнее</Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    )
}

export default MainSliderSectionComponent
