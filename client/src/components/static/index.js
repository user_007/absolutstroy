import ServiceSectionComponent from './ServiceSectionComponent';
import MapSectionComponent from './MapSectionComponent';
import BrandsSectionContent from './BrandsSectionContent';
import MainSliderSectionComponent from './MainSliderSectionComponent';


export const ServiceSection = ServiceSectionComponent;

export const MapSection = MapSectionComponent;

export const BrandsList = BrandsSectionContent;

export const MainSliderSection = MainSliderSectionComponent;
