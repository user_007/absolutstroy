import React from 'react';
import PropTypes from 'prop-types';

import data from '../../temp/main-page-categories';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { 
  faUserClock as clock, faMapMarkerAlt as address, faEnvelopeOpenText as mail, faMobileAlt as phone
} from '@fortawesome/free-solid-svg-icons';

import './Map.css';

const MapSectionComponent = ({ withTabs }) => {
  return (
    <div className="map-container">
      <iframe className="map" title="расположение офиса компании Абсолют-Строй" src="https://yandex.ru/map-widget/v1/?um=constructor%3A2cf50159ab9764cb43195e550c2ae91297148e6514aa9f4e08c9bf25fef31ed2&amp;source=constructor" width="100%" height="600" frameBorder="0"></iframe>
      {
        withTabs && <div className="org-data">
        <div className="org-data--item" data-aos-offset="150" data-aos="fade-up">
          <h3 className="org-data--title"><FontAwesomeIcon className="org-data--ico" icon={ address } /> Адрес:</h3>
          <p className="org-data--content">{ data.contacts.address }</p>
        </div>
        <div className="org-data--item" data-aos-offset="300" data-aos="fade-up">
          <h3 className="org-data--title"><FontAwesomeIcon className="org-data--ico" icon={ mail } /> Эл. почта:</h3>
          <p className="org-data--content">{ data.contacts.email }</p>
        </div>
        <div className="org-data--item" data-aos-offset="450" data-aos="fade-up">
          <h3 className="org-data--title"><FontAwesomeIcon className="org-data--ico" icon={ phone } /> Телефон:</h3>
          <p className="org-data--content">{data.contacts.phone1 }</p>
          <p className="org-data--content">{data.contacts.phone2 }</p>
        </div>
        <div className="org-data--item" data-aos-offset="600" data-aos="fade-up">
          <h3 className="org-data--title"><FontAwesomeIcon className="org-data--ico" icon={ clock } /> Время работы:</h3>
          <p className="org-data--content">пн.-пт.: 8:00 - 20:00</p>
        </div>
      </div>
      }
    </div>
  )
}

MapSectionComponent.propTypes = {
  withTabs: PropTypes.bool.isRequired
}

MapSectionComponent.defaultProps = {
  withTabs: true
}

export default MapSectionComponent;