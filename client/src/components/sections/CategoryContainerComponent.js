import React from 'react';
import PropTypes from 'prop-types';
import { v4 as keyGen } from 'uuid';

import { Link } from 'react-router-dom';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShareSquare } from '@fortawesome/free-solid-svg-icons';

const CategoryContainerComponent = ({ path, linkTitle, description, features, featuresCaption }) => {

    const featuresList = !features ? 
        null : 
        features.map( 
            feature => <li className="feature" key={ keyGen() }>{ feature }</li> 
        );


    const featuresBlock = !features ? null : (
    <ul className="category__features">
        <li className="features-caption">{featuresCaption}</li>
        {featuresList}
    </ul>
    )


    return (
        <div className="category">

            <div className="category__info">

                <h3 className="category__title">
                    <Link to={path}>{linkTitle} <FontAwesomeIcon icon={faShareSquare} /></Link>
                </h3>

                <p className="category__description">{description}</p>

                { featuresBlock }

            </div>

        </div>
    )
}



CategoryContainerComponent.propTypes = {
    path: PropTypes.string.isRequired,
    linkTitle: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    featuresCaption: PropTypes.string,
    features: PropTypes.arrayOf(PropTypes.string)
}


export default CategoryContainerComponent
