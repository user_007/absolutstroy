import CategoryContainerComponent from './CategoryContainerComponent';
import CategoriesImageContainerComponent from './CategoryImageContainerComponent';

export const CategoryContainer = CategoryContainerComponent;

export const CategoryImageContainer = CategoriesImageContainerComponent;
