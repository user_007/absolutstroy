import React from 'react';
import PropTypes from 'prop-types';

const CategoryImageContainerComponent = ({ title, img, subcategories }) => {
    return (
        <div className="category__img-container">
            <img src={img} alt="сип панели" className="category__img" />
            <div className="work-category__details">
                <h2 className="work-category__title">{title}</h2>
                {/* { subcategories && <p className="work-category__item">{subcategories}</p> } */}
            </div>
        </div>
    )
}

CategoryImageContainerComponent.propTypes = {
    title: PropTypes.string.isRequired,
    img: PropTypes.string.isRequired,
    subcategories: PropTypes.string
}

export default CategoryImageContainerComponent
