import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';

import './Loading.css';

function Loading() {
  return (
    <div className="loading-portal">
      <span>
        <FontAwesomeIcon 
          icon={ faSpinner } pulse
        /> Загрузка...
      </span>
    </div>
  )
}

export default Loading;
