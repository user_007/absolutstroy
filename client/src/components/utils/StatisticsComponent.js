import React from 'react';
import Countup from 'react-countup';

import './Stat.css';

const StatisticsComponent = props => {
  return (
    <div className="statistics">
      <div className="stat-block">        
        <span className="stat-block--value"> от <Countup end={500000} duration={3} /></span>
        <strong className="stat-block--title">Доступные цены на все виды домов с поэтапной оплатой</strong>        
      </div>
      <div className="stat-block">        
        <span className="stat-block--value">от <Countup end={45} duration={3} /> дней</span>
        <strong className="stat-block--title">Быстрый монтаж как каркасных домов так и промышленных зданий</strong>        
      </div>
      <div className="stat-block">        
        <span className="stat-block--value">> <Countup end={12} duration={3} /> лет</span>
        <strong className="stat-block--title">Большой опыт в сфере строительства домов по всей России</strong>        
      </div>
      <div className="stat-block">        
        <span className="stat-block--value"><Countup end={1000000} duration={3} /> м<sup>2</sup></span>
        <strong className="stat-block--title">Мы уже построили более 20 объектов различного назначения</strong>        
      </div>

    </div>
  )
}


export default StatisticsComponent;
