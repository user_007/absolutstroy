import React from 'react';
import PropTypes from 'prop-types';



function BrandComponent({ image, name }) {
  return (
    <div className="brand-container">
      <img src={image} className="brand-icon" alt={name} />
    </div>
  )
}


BrandComponent.propTypes = {
  name: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired
}

BrandComponent.defaultProps = {
  name: 'материал'
}

export default BrandComponent;
