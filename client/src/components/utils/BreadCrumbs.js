import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';

import './BreadCrumbs.css';

const BreadCrumbs = ({
  path, title
}) => {
  let links = path.split('/');

  const
    bcConvert = {
      projects: 'проекты',
      contacts: 'контакты',
      materials: 'материалы',
      portfolio: 'фотографии',
      gallery: 'галерея'
    },

    pathLinks = links.filter(link => link !== '');

  if (!Object.keys(bcConvert).includes(pathLinks[pathLinks.length - 1])) pathLinks.length -= 1;

  if (!pathLinks.length) return null;

  const breadCrumbs = pathLinks.map((link, ind) => {
    const path = [...pathLinks],
      title = bcConvert[link] || link;
    let node = null;

    path.length = ind + 1;
    node = (<li className="bc-link">
      <Link        
        className="bc-link"
        to={`/${path.join('/')}`}
      >{title}</Link>
    </li>
    )



    return (
      <Fragment key={ link }>
        <li className="bc-divider">/</li>
        {node}
      </Fragment>
    )
  })


  return (
    <div className="breadcrumbs">
        <ul className="breadCrumbs-list">
          <li><Link className="bc-link" to="/">Главная</Link></li>
          { breadCrumbs }
          { title && <Fragment><li className="bc-divider">/</li>
          <li><span>{title}</span></li></Fragment> }
        </ul>
    </div>
  )
}

export default BreadCrumbs;
