import React from 'react';
import PropTypes from 'prop-types';
import toaster from 'toasted-notes';

import './Notification.css'

const Notification = ({
  title, msg, img, type
}) => {

  let notificationImg;

  if (img) {

    notificationImg = img;

  } else {

    switch (type) {
      case 'info':
        notificationImg = '';
        break;
      case 'success':
        notificationImg = '';
        break;
      case 'warning':
        notificationImg = '';
        break;
      default:
        notificationImg = '';
    }

  }

  return toaster.notify(({ onClose }) => (
    <div className={`notification${' notification-' + type}`}>
      <button onClick={onClose} className="notification__close-btn">x</button>
      <div className="notification__content">
        <img src={notificationImg} alt="иконка уведомления" />
        <div className="notification__body">
          {title && <h3 className="notification__title">{title}</h3>}
          <p className="notification__msg">{msg}</p>
        </div>
      </div>
    </div>
  ))

}


Notification.propTypes = {
  title: PropTypes.string,
  msg: PropTypes.string.isRequired,
  img: PropTypes.string,
  type: PropTypes.oneOf(['success, warning, info']).isRequired,
}


Notification.defaultProps = {
  type: 'info'
}

export default Notification;
