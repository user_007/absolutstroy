import React from 'react';
import PropTypes from 'prop-types';
import { v4 as keyGen } from 'uuid';

import './Gallery.css';

const GalleryComponent = (
  { images, onClick }
) => {

  return (
    <div className="gallery row">

      {
        images.map(
          (img, index) => <div className="gallery--item col-md-4 col-sm-6" key={keyGen()}>
            <img
              src={img.src} 
              data-index={index}
              onClick={onClick}
              alt="процесс строительства дома"
            />
          </div>
        )
      }

    </div>
  )
}

GalleryComponent.propTypes = {
  images: PropTypes.arrayOf(PropTypes.shape({
    src: PropTypes.string.isRequired
  })).isRequired
}

export default GalleryComponent;
