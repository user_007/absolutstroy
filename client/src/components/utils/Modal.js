import React from 'react';
import ReactModal from 'react-modal';
import PropTypes from 'prop-types';

import './Modal.css';

const Modal = ({
  isOpen, handleModalClose, contentLabel, onRequestClose, onAfterOpen, style, children
}) => {
  ReactModal.setAppElement('#root');

  return (
    <ReactModal
      style={style}
      isOpen={isOpen}
      contentLabel={contentLabel}
      onRequestClose={onRequestClose}
      onAfterOpen={onAfterOpen}
    >
      <button onClick={handleModalClose} className="ReactModal__close-btn"
      >×</button>
      {children}
    </ReactModal>
  );
}

Modal.propTypes = {
  isOpen: PropTypes.bool.isRequired, 
  contentLabel: PropTypes.string, 
  handleModalClose: PropTypes.func.isRequired,
  onRequestClose: PropTypes.func, 
  onAfterOpen: PropTypes.func, 
  style: PropTypes.object
}

Modal.defaultProps = {
  isOpen: false
}

Modal.defaultProps = {
  style: {
    overlay : {
      position         : 'null',
      top              : 'null',
      left             : 'null',
      right            : 'null',
      bottom           : 'null',
      display          : 'null',
      alignItems       : 'null',
      justifyContent   : 'null',
      backgroundColor  : 'null'
    },
    content : {
      position    : 'null',
      top         : 'null',
      left        : 'null',
      right       : 'null',
      bottom      : 'null',
      border      : 'null',
      background  : 'null',
      padding     : 'null',
      maxWidth    : 'null',
      minWidth    : 'null'
    }
  }
}

export default Modal;
