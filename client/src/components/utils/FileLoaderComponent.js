import React from 'react';
import PropTypes from 'prop-types';

import Loading from "./Loading";

import { loadFileWithPreview } from '../../utils/commonFunctions';

import './FileLoader.css';



const FileLoaderComponent = ({
  id, name, label, info, disabled, handleUploadPhoto
}) => {
  const filesInput = React.createRef();

  const triggerFileLoading = e => {
    e.preventDefault();
    // handleUploadPhoto
    filesInput.current.click();
  }

  const getFile = async e => {
    e.preventDefault();

    // const { files } = filesInput.current;

    // const fileObject = await loadFileWithPreview()
  }


  const uploadBtn = disabled 
    ? (
      <button
        disabled={disabled}
        onClick={undefined}
        className="btn btn-info btn-block btn-sm disabled mt-2"
      >
        <Loading />
      </button>
    ) : (
      <button
        disabled={disabled}
        className="btn btn-info btn-block btn-sm mt-2"
        onClick={triggerFileLoading}
      >
        <i className="fa fa-image fa-fw" /> Загрузить фото
      </button>
    );
  


  return (
    <div className="container">
      <div className="control__header">
        {label && <h4 className="control__title">{label}</h4>}
        {info && <p className="control__subtitle">{info}</p>}
      </div>
      {/* <img src={} alt="файл"/> */}
      <div className="multi-file-controls">


        <input
          id={id}
          disabled={disabled}
          type="file"
          ref={filesInput}
          className={`js-load-photo hidden${disabled ? ' disabled' : ''}`}
          name={name}
          onChange={disabled ? undefined : getFile}
          // files={files}
        />

        {/* <div className={`alert ${error ? 'alert-danger' : 'alert-info'} multi-file__alert`}>
            <p className="m-0">{error ? error : filesList.trim()}</p>
          </div> */}

        {/* {
          files.length === 0 &&
          <div className="alert alert-info multi-file__alert mt-2">
            <p className="m-0">Файл не выбран...</p>
          </div>
        } */}

        {uploadBtn}
      </div>
      <div className="clearfix"></div>
    </div>
  )
}

FileLoaderComponent.propTypes = {
  label: PropTypes.string, info: PropTypes.string, disabled: PropTypes.bool, handleUploadPhoto: PropTypes.func
}

export default FileLoaderComponent
