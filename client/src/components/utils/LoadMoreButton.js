import React from 'react'
import PropTypes from 'prop-types';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner, faImages } from '@fortawesome/free-solid-svg-icons';


const LoadMoreButton = ({
    loading, onClick, showButtonCondition
}) => {

    const button = showButtonCondition
            ?   <div>
                    <button 
                        className="btn btn-info"
                        disabled={ loading }
                        onClick={ onClick }
                    >
                        {
                            loading 
                                ? <React.Fragment><FontAwesomeIcon icon={ faSpinner } spin /> Загрузка...</React.Fragment>
                                : <React.Fragment><FontAwesomeIcon icon={ faImages } /> Загрузить еще</React.Fragment>
                        }
                    </button>
                </div>

            :   null;
            

    return button;
}

LoadMoreButton.propTypes = {
    loading: PropTypes.bool.isRequired,
    showButtonCondition: PropTypes.bool.isRequired,
    onClick: PropTypes.func.isRequired
}

export default LoadMoreButton
