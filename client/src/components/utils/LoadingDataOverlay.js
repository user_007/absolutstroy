import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';

import './Loading.css';

const LoadingDataOverlay = () => {
  return (
    <div className="loading-overlay">
      <span><FontAwesomeIcon icon={ faSpinner } spin /> Сохранение проекта...</span>
    </div>
  )
}


export default LoadingDataOverlay;
