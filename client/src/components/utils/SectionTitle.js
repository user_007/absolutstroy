import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';

const SectionTitle = ({ title }) => {

  const titleWords = title.split(' ');

  const leftLength = Math.ceil(titleWords.length / 2);

  const leftPart = _.take(titleWords, leftLength);
  const rightPart = _.takeRight(titleWords, titleWords.length - leftLength);
  
  return <h2 className="section__title">{leftPart.join(' ')}<span className="title_secondary"> {rightPart.join(' ')}</span></h2>;
  
}

SectionTitle.propTypes = {
  title: PropTypes.string.isRequired
}

SectionTitle.defaultProps = {
  title: ''
}

export default SectionTitle;
