import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import data from '../temp/main-page-categories';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faVk, faFacebookF, faInstagram } from '@fortawesome/free-brands-svg-icons';

import './Footer.css';


class FooterComponent extends Component {
  render() {
    return (
      //https://www.weblancer.net/users/keyline/portfolio/dizajn-sajtov-9/sajt-pod-klyuch-na-dvizhke-opencart-2035975/
      <div className="footer">
        <div className="container" itemScope itemType="http://schema.org/HomeAndConstructionBusiness">
          <meta itemProp="name" content="Абсолют Строй 92" /> 
          <meta itemProp="slogan" content="Абсолют Строй 92 - Ваш надежный партнер в строрительстве домов под ключ" /> 
          <meta itemProp="areaServed" content="Севастополь" /> 
          <div className="row">
            <div className="col-sm-6 col-md-4 footer-block">
              <h4 className="block--title">О компании</h4>
              <p itemProp="description" className="pt-2 text-sm">{data.company.about}</p>
              <ul className="snw-list">
                <li>
                  <a className="snw--link" rel="noopener noreferrer" target="_blank" href="http://www.vk.com">
                    <FontAwesomeIcon icon={faVk} />
                  </a>
                </li>
                <li>
                  <a className="snw--link" rel="noopener noreferrer" target="_blank" href="http://www.facebook.com">
                    <FontAwesomeIcon icon={faFacebookF} />
                  </a>
                </li>
                <li>
                  <a className="snw--link" rel="noopener noreferrer" target="_blank" href="http://www.instagram.com">
                    <FontAwesomeIcon icon={faInstagram} />
                  </a>
                </li>
              </ul>
            </div>
            <div className="col-sm-6 col-md-2 footer-block">
              <h4 className="block--title">Навигация</h4>
              <ul className="footer-menu-list">
                <li>
                  <Link to="/projects">проекты</Link>
                </li>
                <li>
                  <Link to="/gallery">галерея</Link>
                </li>
                <li>
                  <Link to="/contacts">контакты</Link>
                </li>
                <li>
                  <Link to="/sitemap.xml">карта сайта</Link>
                </li>
              </ul>
            </div>
            <div className="col-sm-6 col-md-3 footer-block">
              <h4 className="block--title">Контакты</h4>
              <ul className="footer-contact-list">
                <li>
                    адрес: <span itemProp="address">{ data.contacts.address }</span>
                </li>
                <li>
                  <a href="callto:+79780131703" target="_blank" rel="noopener noreferrer">
                    телефон: <span itemProp="telephone">{ data.contacts.phone1 }</span>
                  </a>
                </li>
                <li>
                  <a href="callto:+79782553035" target="_blank" rel="noopener noreferrer">
                    телефон: <span itemProp="telephone">{ data.contacts.phone2 }</span>
                  </a>
                </li>
                <li>
                  <a href="mailto:pochta@pochta.net" target="_blank" rel="noopener noreferrer">
                    e-mail: <span itemProp="email">{data.contacts.email}</span>
                  </a>
                </li>
              </ul>
            </div>
            <div className="col-sm-6 col-md-3 footer-block">
              <h4 className="block--title">Время работы</h4>
              <p className="worktime">Наш офис открыт для Вас:<br />пн-пт <span itemProp="openingHours">с 9:00 до 18:00</span></p>
              <p className="text-sm">На время работы монтажников и проектировщиков это никак не распространяется. В зависимости от проекта, они могут работать даже 24/7</p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default FooterComponent;
