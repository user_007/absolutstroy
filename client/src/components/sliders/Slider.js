import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Slick from 'react-slick';

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block", background: "red" }}
      onClick={onClick}
    />
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block", background: "green" }}
      onClick={onClick}
    />
  );
}

class Slider extends Component {
  static propTypes = {
    settings: PropTypes.object,
    slides: PropTypes.array.isRequired
  }

  render() {
    const defaultSettings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      nextArrow: <SampleNextArrow />,
      prevArrow: <SamplePrevArrow />
    }

    const { slides } = this.props;
    const settings = this.props.settings || defaultSettings;

    const content = slides.map( (slide, key) => {
      return (
        <div className="slider__slide.project-slide" key={key}>
          <img src={slide.img} alt={ slide.title } className="project-slide__img" />
          <ul className="project-slide__data-list">
            <li className="project-slide__data-item project-slide_name">{ slide.title }</li>
            <li className="project-slide__data-item project-slide_category">{ slide.category }</li>
          </ul>
        </div>
      )
    });

    return (
      <Slick 
        settings={ settings }
        className="slider project_slider"
      >
        { content }
      </Slick>
    )
  }
}

export default Slider;
