import React from 'react';
import Slider from 'react-slick';
import { v4 as genKey } from 'uuid';

import './sliders.css';

const ProjectSlideComponent = ({ images }) => {

    const mainSliderSettings = {
      dots: true,
      arrows: false,
      slidesToShow: 1,
      infinite: false,
      centerMode: true,
      centerPadding: '0px',
      className: "slider-with-nav",
      appendDots: dots => (
        <div className="project-slider__dots-container">
          <ul className="project-slider__dots-list"> {dots} </ul>
        </div>
      ),
      customPaging: i => (
        <div className="slide-preview">
          <img src={images[i]} alt="фотографии проекта" className="mh-100 mw-100" />
        </div>
      )
    }
    

    const sliderContent = images.map(img => (
      <div className="slide-container" key={ genKey() }>
        <img src={img} alt="фотографии проекта" className="mh-100 mw-100" />
      </div>
    ))

    return (
        <Slider {...mainSliderSettings} >{sliderContent}</Slider>
    );

}

export default ProjectSlideComponent;
