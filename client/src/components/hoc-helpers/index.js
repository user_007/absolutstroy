import withRightsCheck from './withRightsCheck';
import withData from './withData';

export {
    withRightsCheck,
    withData
}