import React, { Component } from 'react';

// import { checkUserRights } from '../../actions/authActions';

import Loading from '../utils/Loading';


const withData = (Wraped, getData) => {
    return class extends Component {

        state = {
            data: null,
            loading: true,
            errors: {}
        }


        async componentDidMount() {

            const response = await getData();

            if (!response.success) {

                if (response.status >= 500) return this.props.history.push("/errors/server-error");

                return this.setState({
                        errors: response.data.errors,
                        loading: false
                       });
            }
    
            this.setState({
                data: response.data,
                loading: false
            });
        }


        render() {

            const { data, loading, errors } = this.state;

            return loading ? <Loading /> : <Wraped { ...this.props } errors={ errors } data={ data } />;
            
        }
    }
}


export default withData;
