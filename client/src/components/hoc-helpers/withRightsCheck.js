import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { checkUserRights } from '../../actions/authActions';

import Loading from '../utils/Loading';


const withRightsCheck = (Wraped) => {
    return class extends Component {

        state = {
            isAllowed: null,
            loading: true
        }


        async componentDidMount() {

            const isAllowed = await checkUserRights(['admin']);
    
            this.setState({
                isAllowed,
                loading: false
            })
        }


        render() {

            const { isAllowed, loading } = this.state;

            if (loading) return <Loading />

            return isAllowed ? <Wraped { ...this.props } /> : <Redirect to="/admin/login" />;
            
        }
    }
}


export default withRightsCheck;
