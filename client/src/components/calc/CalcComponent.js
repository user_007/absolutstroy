import React, { Component } from 'react';
import Modal from '../utils/Modal';
import OrderForm from '../forms/OrderFormComponent';
import Rangeslider from 'react-input-range';
import { v4 as keyGen } from 'uuid';
import toaster from 'toasted-notes';

import _ from 'lodash';

import Image from '../../temp/images';

import { getListOfBasements, getListOfRoofs, getListOfWalls } from '../../actions/materialActions';
import { sendOrder } from '../../actions/ordersActions';

import './CalcComponent.css';

class CalcComponent extends Component {
  state = {
    modalShow: false,
    calcFields: {
      basement: 0,
      roof: 0,
      walls: 0,
      floors: 1,
      height: 2.75,
      area: 60
    },
    calcLists: {
      //each element is an array of objects with name and price fields
      walls: [],
      basements: [],
      roofs: []
    },
    sendingRequest: false,
    order: {
      person: '',
      phone: '',
      comment: ''
    },
    errors: {}
  }

  handleOrderFieldChange = e => {
    const { order } = this.state;

    this.setState({
      order: {
        ...order,        
        [e.target.name]: e.target.value
      },
      errors: _.omit(this.state.errors, [e.target.name])
    });
  }
  

  onChange = e => {
    this.setState({
      calcFields: {
        ...this.state.calcFields,
        [e.target.name]: e.target.value
      }
    })
  };


  openOrderForm = e => {
    e.preventDefault();
    this.setState({ modalShow: true });
  }


  async componentDidMount() {

    try {
      
      const [basements, roofs, walls] = await Promise.all([
        getListOfBasements(), getListOfRoofs(), getListOfWalls()
      ]);
  
      this.setState({
        calcLists: {
          basements, roofs, walls
        }
      });

    } catch (err) {

      console.log('Не удалось загрузить списки...', err);
      
    }    
    
  }


  createOrder = () => {
    const {
      calcFields: Field, calcLists: List
    } = this.state;

    const basementPrice = List.basements.length ? Math.floor(List.basements[Field.basement].price * Field.area) : 0;
    const roofPrice = List.roofs.length ? Math.floor(List.roofs[Field.roof].price * Field.area * 1.4) : 0;
    const wallsPrice = List.walls.length ? Math.floor(Math.sqrt(Field.area) * 5 * List.walls[Field.walls].price * Field.floors * Field.height) : 0;
    const total = basementPrice + roofPrice + wallsPrice;

    const data = {
      type: 'calc',
      walls: List.walls.length ? List.walls[Field.walls].material : 'не выбран',
      basement: List.basements.length ? List.basements[Field.basement].material : 'не выбран',
      roof: List.roofs.length ? List.roofs[Field.roof].material : 'не выбран',
      floors: Field.floors,
      area: Field.area,
      height: Field.height,
      prices: {
        basement: basementPrice,
        walls: wallsPrice,
        roof: roofPrice,
        total: total
      }      
    }

    return {
      data: data,
      details: `<b>Детали заявки:</b><br />
      стены: ${data.walls}, фундамент: ${data.basement}, крыша: ${data.roof}, площадь фундамента: ${data.area} кв. м, кол-во этажей: ${data.floors}, высота потолков: ${data.height} м
              <br /><br />
      <b>Стоимость:</b> <br />
      фундамент: ${data.prices.basement} руб., монтаж стен: ${data.prices.walls} руб., монтаж кровли: ${data.prices.roof} руб.
              <br /><hr />
      <b>Итого: ${data.prices.total} руб.</b>`
    }

  }


  submitOrderForm = async e => {
    e.preventDefault();

    this.setState({ sendingRequest: true });

    const { order } = this.state;

    const result = await sendOrder({ 
      ...order, type: 'calc', details: this.createOrder().details 
    });

    let notification;

    if (!result.success) {
      const { errors } = result.data;

      if (errors.person || errors.phone)
        return this.setState({
          errors: errors,
          sendingRequest: false
        });

      notification = 'не удалось отправить заявку...';

    } else {

      notification = 'Заявка успешно отправлена';

    }

    this.setState({
      modalShow: false,
      sendingRequest: false,
      order: {
        person: '',
        phone: '',
        comment: ''
      }
    });

    toaster.notify(notification, { duration: 5000, position: 'bottom-right' });

  }
  

  render() {

    const { 
      calcFields: { ...Field }, 
      calcLists: { ...List }, 
      order: { person, comment, phone }
    } = this.state;


    const wallsList = List.walls.length === 0 ? null : List.walls.map((walls, index) => {
      return (
        <option value={index} key={index}>{walls.material}</option>
      )
    });

    const roofsList = List.roofs.length === 0 ? null : List.roofs.map((roof, index) => {
      return (
        <option value={index} key={index}>{roof.material}</option>
      )
    });

    const basementsList = List.basements.length === 0 ? null : List.basements.map((basement, index) => {
      return (
        <option value={index} key={index}>{basement.material}</option>
      )
    });

    const { data: order, details: alert } = this.createOrder();



    return (
      <div className="container">
        <Modal 
          isOpen={ this.state.modalShow } 
          handleModalClose={ () => this.setState({ modalShow: false }) }
        >
          <OrderForm 
            fields={ {person, comment, phone} } 
            errors={ this.state.errors }
            handleChange={ this.handleOrderFieldChange }
            handleSubmit={ this.submitOrderForm }
            title="Заявка по предварительным расчетам" alert={ alert }
          />
        </Modal>

        <div className="row">
          <div className="col-md-6">

            <form action="calc" className="calc-form" onSubmit={ this.openOrderForm.bind(this) }>
              <h3 className="calc-form__title">Расчитать стоимость</h3>
              <div className="row">
                <div className="col-sm-6 form-group form-group_calc">
                  <label htmlFor="walls" className="control-label">Тип материала</label>
                  <select
                    name="walls" id="walls"
                    className="form-control"
                    value={Field.walls}
                    onChange={this.onChange.bind(this)}
                  >
                    {wallsList}
                  </select>
                </div>

                <div className="col-sm-6 form-group form-group_calc">
                  <label htmlFor="basement" className="control-label">Тип фундамента</label>
                  <select
                    name="basement" id="basement"
                    className="form-control"
                    value={Field.basement}
                    onChange={this.onChange.bind(this)}
                  >
                    {basementsList}
                  </select>
                </div>                
              </div>

              <div className="row">
                <div className="col-sm-6 form-group form-group_calc">
                  <label htmlFor="roof" className="control-label">Тип кровли</label>
                  <select
                    name="roof" id="roof"
                    className="form-control"
                    value={Field.roof}
                    onChange={this.onChange.bind(this)}
                  >
                    {roofsList}
                  </select>
                </div>
                <div className="col-sm-4 form-group form-group_calc">
                  <label htmlFor="floors" className="control-label">Кол-во этажей</label>
                  <select
                    type="text" name="floors" id="floors"
                    className="form-control"
                    value={Field.floors}
                    onChange={this.onChange.bind(this)}
                  >
                    { [1, 2, 3, 4].map( num => <option value={num} key={ keyGen() }>{num}</option> ) }
                  </select>
                  
                </div>
              </div>

              <div className="row">                
                <div className="col-sm-6 form-group form-group_calc">
                  <label htmlFor="height" className="control-label">Высота потолков</label>
                  {/* <input
                    type="text" name="height" id="height"
                    className="form-control"
                    value={Field.height}
                    onChange={this.onChange.bind(this)}
                  /> */}
                  <div className="slider-horizontal rangeslider rangeslider_light">
                  <Rangeslider 
                    name="height" id="height" className="form-control"
                    minValue={2.2} maxValue={4} step={0.05}
                    value={Number(Field.height)}
                    formatLabel={ val => `${val} м` }
                    onChange={ val => this.setState({ 
                      calcFields: {
                        ...Field,
                        height: val.toFixed(2)
                        }
                      }) 
                    }
                  />
                  </div>
                </div>
                <div className="col-sm-6 form-group form-group_calc">
                  <label htmlFor="area" className="control-label">Площадь</label>
                  {/* <input
                    type="text" name="area" id="area"
                    className="form-control"
                    value={Field.area}
                    onChange={this.onChange.bind(this)}
                  /> */}
                  <div className="slider-horizontal rangeslider rangeslider_light">
                  <Rangeslider 
                    name="area" id="area" className="form-control"
                    minValue={40} maxValue={300} step={1}
                    value={Number(Field.area)}
                    formatLabel={ val => <span>{val} м<sup><small>2</small></sup></span> }                 
                    onChange={ val => this.setState({ 
                      calcFields: {
                        ...Field,
                        area: val
                      }
                    }) 
                  }
                  />
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col-sm-6 form-group mt-3 form-group_calc">
                  <button
                    className="btn btn-outline-light btn-block"
                    // eslint-disable-next-line
                    disabled={Field.height <= 0 || Field.area < 1 || Field.floors < 1}
                  >Заказать</button>
                </div>
                <div className="col-sm-6">
                </div>
              </div>
            </form>

          </div>


          <div className="col-md-6">
            <div className="calc-data">
              <div className="calc-data__top">
                <img src={ Image.Calc.TOP } alt="расчеты заказа"/>
              </div>

              <div style={{ position: "relative" }}>
              <img 
                  className="calc-bg" 
                  src={Image.Calc.MIDDLE} 
                  alt="данные расчетов калькулятора"
                />
              <ul className="calc-data__list">                
                <li className="calc-data__value">
                  Тип материала:
                  
                  <span>{order.walls}</span>
                </li>
                <li className="calc-data__value">
                  Тип фундамента:
                  
                  <span>{order.basement}</span>
                </li>
                <li className="calc-data__value">
                  Тип кровли:
                  
                  <span>{order.roof}</span>
                </li>
                <li className="calc-data__value">
                  Количество этажей:
                  
                  <span>{order.floors}</span>
                </li>
                <li className="calc-data__value">
                  Высота потолков:
                  
                  <span>{order.height} м</span>
                </li>
                <li className="calc-data__value">
                  Площадь:
                  
                  <span>{order.area} кв. м</span>
                </li>
                <li className="calc-data__devider"></li>
                <li className="calc-data__value value_total">
                  Стоимость фундамента:<span>{order.prices.basement} руб.</span>
                </li>
                <li className="calc-data__value value_total">
                  Стоимость стен:<span>{order.prices.walls} руб.</span>
                </li>
                <li className="calc-data__value value_total">
                  Стоимость кровли:
                  
                  <span>{order.prices.roof} руб.</span>
                </li>
                <li className="calc-data__devider"></li>
                <li className="calc-data__value value_total">
                  Итого: <span>{order.prices.total} руб.</span>
                </li>
              </ul>
              </div>
              {/* <button className="btn btn-warning btn-block hidden">Заказать</button> */}
            </div>

          </div>
        </div>
      </div>
    )
  }
}

export default CalcComponent;
